package com.live.productactivation.rest;

import com.google.gson.JsonObject;
import com.live.productactivation.model.GetTopUsers;
import com.live.productactivation.model.LoginDetails;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Login {
    @FormUrlEncoded
    @POST("Login")
    Call<LoginDetails> getlogindetails(@Field("UserId") String UserId, @Field("profile_picture") String profile_picture);

    @Headers({"Accept: application/json"})
    @POST("UpdateCoin")
    Call<GetTopUsers> getUpdateCoins(@Header("token") String token,@Body JsonObject object);

    @Headers({"Accept: application/json"})
    @GET("GetTopUsers")
    Call<GetTopUsers> getGetTopUsers(@Header("token") String token);

    @Headers({"Accept: application/json"})
    @POST("InsertBet")
    Call<GetTopUsers> getInsertBet(@Header("token") String token,@Body JsonObject object);
}
