package com.live.productactivation.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginAPI {
    Gson gson = new GsonBuilder().setLenient().create();

    private static <T> T builder(Class<T> endpoint) {
        return new Retrofit.Builder()
                .baseUrl("http://208.64.33.80:5002/User/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(endpoint);

    }

    public static Login login() { return builder(Login.class); }
}
