package com.live.productactivation.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

    Gson gson = new GsonBuilder().setLenient().create();

    private static <T> T builder(Class<T> endpoint) {
        return new Retrofit.Builder()
                .baseUrl("http://3.14.135.98:8080/service/d2n/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(endpoint);

    }

    public static MatchList matchList() { return builder(MatchList.class); }

    public static GraphList graphlist() { return builder(GraphList.class); }

}
