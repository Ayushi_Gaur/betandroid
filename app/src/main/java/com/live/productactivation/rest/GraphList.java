package com.live.productactivation.rest;

import com.live.productactivation.model.CommonTime;
import com.live.productactivation.model.QuestionsRelated;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface GraphList {

    @GET("getapidata/{matchid}")
    Call<CommonTime.JsonDetails> getquestionsrelateddetails(@Path("matchid") String matchid);


}
