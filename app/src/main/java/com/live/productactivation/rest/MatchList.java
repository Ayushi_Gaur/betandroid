package com.live.productactivation.rest;

import com.live.productactivation.model.MatchListDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MatchList {

    @GET("gettopgames")
    Call<List<MatchListDetail>> getmatchlistdetails();

}
