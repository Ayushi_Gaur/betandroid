package com.live.productactivation.common;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {

    SharedPreferences mPref;
    SharedPreferences.Editor editor;

    public PrefManager(Context mContext) {
        mPref = mContext.getSharedPreferences("CountDown", Context.MODE_PRIVATE);
        editor = mPref.edit();
    }

    public void setPerDayCoin(int perDayCoin) {
        if (mPref != null) {
            editor = mPref.edit();
            if (perDayCoin != 0)
                editor.putInt("perDayCoin", perDayCoin);
            editor.apply();
        }
    }

    public int getPerDayCoin() {
        int var = 0;
        if (mPref != null) {
            var = mPref.getInt("perDayCoin", 0);
        }
        return var;
    }


    public void setTotalCoins(int totalCoins) {
        if (mPref != null) {
            editor = mPref.edit();
            if (totalCoins != 0)
                editor.putInt("totalCoins", totalCoins);
            editor.apply();
        }
    }

    public int getTotalCoins() {
        int var = 0;
        if (mPref != null) {
            var = mPref.getInt("totalCoins", 0);
        }
        return var;
    }

    public void setUserName(String userName) {
        if (mPref != null) {
            editor = mPref.edit();
            if (userName != "")
                editor.putString("userName", userName);
            editor.apply();
        }
    }

    public String getUserName() {
        String var = null;
        if (mPref != null) {
            var = mPref.getString("userName", "");
        }
        return var;
    }

    public void setUserPhoto(String userPhoto) {
        if (mPref != null) {
            editor = mPref.edit();
            if (userPhoto != "")
                editor.putString("userPhoto", userPhoto);
            editor.apply();
        }
    }

    public String getUserPhoto() {
        String var = null;
        if (mPref != null) {
            var = mPref.getString("userPhoto", "");
        }
        return var;
    }
    public void setUserToken(String userToken) {
        if (mPref != null) {
            editor = mPref.edit();
            if (userToken != "")
                editor.putString("userToken", userToken);
            editor.apply();
        }
    }

    public String getUserToken() {
        String var = null;
        if (mPref != null) {
            var = mPref.getString("userToken", "");
        }
        return var;
    }
}
