package com.live.productactivation.common;

import android.os.Handler;

public class Scheduler {

    // DATA
    private OnScheduleTimeListener mListener;
    private Handler mHandler;
    private int mInterval;          // Between each executions
    private static final int DELAY = 100;      // before first execution
    private boolean mIsTimerRunning;

    public static interface OnScheduleTimeListener {

        public void onScheduleTime();
    }

    public Scheduler(int interval) {
        super();
        mInterval = interval;
        mHandler = new Handler();
    }

    private final Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            // Do stuff
            mListener.onScheduleTime();
            // Repeat
            mHandler.postDelayed(mRunnable, mInterval);
        }
    };

    public void setOnScheduleTimeListener(OnScheduleTimeListener listener) {
        mListener = listener;
    }

    public void startTimer() {
        mIsTimerRunning = true;
        mHandler.postDelayed(mRunnable, DELAY);
    }

    public void stopTimer() {
        mIsTimerRunning = false;
        mHandler.removeCallbacks(mRunnable);
    }

    public boolean isTimerRunning() {
        return mIsTimerRunning;
    }
}
