package com.live.productactivation.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.live.productactivation.model.Last3Bets;
import com.live.productactivation.model.QuestionsResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "MyBet";
    // Channel table name
    public static final String TABLE_BETRESULT = "MyBetResult";
    // Channel Table Columns names
    public static final String BET_ID = "bet_id";
    public static final String MATCH_ID = "match_id";
    public static final String MATCH_ODDTIME = "match_oddtime";
    public static final String MATCH_QUE_NUMBER = "match_que_number";
    public static final String MATCH_RESULT_NUMBER = "match_result_number";
    public static final String MATCH_RESULT_NAME = "match_result_name";
    public static final String MATCH_BET_COINS = "match_bet_coins";
    public static final String MATCH_BET_RESULT = "match_bet_result";

    private static String CREATE_TABLE1;

    private static final String LAST_3_BETS_TABLE = "last3bets";

    private static final String ID = "id";
    private static final String GAME_NAME = "gameName";
    private static final String ODD_TIME = "oddtime";
    private static final String NET_NAME = "netName";
    private static final String RESULTS_NAME = "resultName";
    private static final String BET_COINS = "betCoins";

    private ContentValues cValues;
    private SQLiteDatabase sqLiteDatabase = null;
    private Cursor cursor;

    private String CREATE_BETRESULT_TABLE;

    private String DROP_BETRESULT_TABLE = "DROP TABLE IF EXISTS " + TABLE_BETRESULT;

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        CREATE_BETRESULT_TABLE = "CREATE TABLE " + TABLE_BETRESULT + "("
                + BET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + MATCH_ID + " TEXT,"
                + MATCH_ODDTIME + " TEXT,"
                + MATCH_QUE_NUMBER + " TEXT,"
                + MATCH_RESULT_NUMBER + " TEXT,"
                + MATCH_RESULT_NAME + " TEXT,"
                + MATCH_BET_COINS + " TEXT ,"
                + MATCH_BET_RESULT + " TEXT )";

        CREATE_TABLE1 = "CREATE TABLE " + LAST_3_BETS_TABLE + "(" + ID + " INTEGER PRIMARY KEY autoincrement, "
                + GAME_NAME + " TEXT ,"
                + ODD_TIME + " TEXT ,"
                + NET_NAME + " TEXT ,"
                + RESULTS_NAME + " TEXT ,"
                + BET_COINS + " TEXT)";

        db.execSQL(CREATE_TABLE1);
        db.execSQL(CREATE_BETRESULT_TABLE);
        Log.e("CREATE TABLE-------", "BETRESULT TABLE CREATED...........");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_BETRESULT_TABLE);
        db.execSQL(LAST_3_BETS_TABLE);
        onCreate(db);
    }

    public void insertBetResult(String matchId, String oddTime, String queNumber, String resNumber, String resName, String betCoins, String betResult) {
        sqLiteDatabase = getWritableDatabase();
        cValues = new ContentValues();
        cValues.put(MATCH_ID, matchId);
        cValues.put(MATCH_ODDTIME, oddTime);
        cValues.put(MATCH_QUE_NUMBER, queNumber);
        cValues.put(MATCH_RESULT_NUMBER, resNumber);
        cValues.put(MATCH_RESULT_NAME, resName);
        cValues.put(MATCH_BET_COINS, betCoins);
        cValues.put(MATCH_BET_RESULT, betResult);
        sqLiteDatabase.insert(TABLE_BETRESULT, null, cValues);
        sqLiteDatabase.close();
    }

    public List<QuestionsResult> getAllBetResult() {
        sqLiteDatabase = getReadableDatabase();
        List<QuestionsResult> questionsResultArrayList = new ArrayList<>();//bol
        String query = "SELECT * FROM " + TABLE_BETRESULT;
        cursor = sqLiteDatabase.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                QuestionsResult questionsResult = new QuestionsResult();
                questionsResult.setMatchID(cursor.getString(1));
                questionsResult.setOddtime(cursor.getString(2));
                questionsResult.setQueNumber(cursor.getString(3));
                questionsResult.setResultNumber(cursor.getString(4));
                questionsResult.setResultName(cursor.getString(5));
                questionsResult.setBetCoins(cursor.getString(6));
                questionsResult.setBetResult(cursor.getString(7));
                questionsResultArrayList.add(questionsResult);
            } while (cursor.moveToNext());
        }

        cursor.close();
        sqLiteDatabase.close();
        return questionsResultArrayList;
    }

    public void updateBetResult(String match_id, String match_oddtime, String match_queNumber, String match_result_name, String value) {
        sqLiteDatabase = getWritableDatabase();
        cValues = new ContentValues();
        cValues.put(MATCH_BET_RESULT, value);
        sqLiteDatabase.update(DBHandler.TABLE_BETRESULT, cValues,
                MATCH_ID + "='" + match_id + "' AND" + MATCH_ODDTIME + "='" + match_oddtime + "' AND" + MATCH_QUE_NUMBER + "='" + match_queNumber + "' AND" + MATCH_RESULT_NAME + "='" + match_result_name + "'", null);
        sqLiteDatabase.close();
    }


    public void insertBettingDetails(String gameName, String oddtime, String netName, String resultName, String betCoins) {
        sqLiteDatabase = getWritableDatabase();
        cValues = new ContentValues();
        cValues.put(GAME_NAME, gameName);
        cValues.put(ODD_TIME, oddtime);
        cValues.put(NET_NAME, netName);
        cValues.put(RESULTS_NAME, resultName);
        cValues.put(BET_COINS, betCoins);
        sqLiteDatabase.insert(LAST_3_BETS_TABLE, null, cValues);
        sqLiteDatabase.close();
    }

    public List<Last3Bets> getBettingDetails() {
        sqLiteDatabase = getReadableDatabase();
        cursor = sqLiteDatabase.rawQuery("select * from " + LAST_3_BETS_TABLE, null);
        List<Last3Bets> list = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                list.add(new Last3Bets(cursor.getString(cursor.getColumnIndex(GAME_NAME)), cursor.getString(cursor.getColumnIndex(ODD_TIME)), cursor.getString(cursor.getColumnIndex(NET_NAME)), cursor.getString(cursor.getColumnIndex(RESULTS_NAME)), cursor.getString(cursor.getColumnIndex(BET_COINS))));
            } while (cursor.moveToNext());
        }

        Collections.reverse(list);
        cursor.close();
        sqLiteDatabase.close();
        return list;
    }

}

