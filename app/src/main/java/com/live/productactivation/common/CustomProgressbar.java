package com.live.productactivation.common;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.live.productactivation.R;

public class CustomProgressbar extends Dialog {
    private static CustomProgressbar mCustomProgressbar;
    private CustomProgressbar mProgressbar;
    private OnDismissListener mOnDissmissListener;

    private CustomProgressbar(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_progressbar);
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mOnDissmissListener != null) {
            mOnDissmissListener.onDismiss(this);
        }
    }

    public static void showProgressBar(Context context, boolean cancelable) {
        showProgressBar(context, cancelable, null);
    }

    public static void showProgressBar(Context context, boolean cancelable, String message) {
        if (mCustomProgressbar != null && mCustomProgressbar.isShowing()) {
            mCustomProgressbar.cancel();
        }
        mCustomProgressbar = new CustomProgressbar(context);
        mCustomProgressbar.setCancelable(cancelable);
        mCustomProgressbar.show();

    }



    public static void hideProgressBar() {
        if (mCustomProgressbar != null) {
            mCustomProgressbar.dismiss();
        }
    }

}