package com.live.productactivation.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.live.productactivation.R;
import com.live.productactivation.model.CommonTime;
import com.live.productactivation.model.MatchListDetail;
import com.live.productactivation.model.QuestionsRelated;
import com.live.productactivation.model.QuestionsResult;
import com.live.productactivation.rest.API;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class BaseActivity extends AppCompatActivity {

    public GoogleSignInClient googleSignInClient;
    public FirebaseAuth mAuth;

    public static final int delay = 0; // delay for 0 sec.
    public static final int period = 15000; // repeat every 15 sec.

    private static ProgressDialog progressDialog;
    DBHandler dbHandler;
    PrefManager prefManager;
    public List<QuestionsRelated> questionsRelatedArrayList = new ArrayList<>();
    public List<MatchListDetail> matchListDetail = new ArrayList<>();
//    int i, j;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mAuth = FirebaseAuth.getInstance();

        googleSignInClient = GoogleSignIn.getClient(this, gso);

        dbHandler = new DBHandler(this);
        prefManager = new PrefManager(this);
    }

    public List<QuestionsRelated> getQuestionsRelatedDetail(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;
                questionsRelatedArrayList = jsonDetails.getQuestionsRelated();

                if (questionsRelatedArrayList != null && questionsRelatedArrayList.size() > 0) {
                    //Toast.makeText(BaseActivity.this, "" + questionsRelatedArrayList.size(), Toast.LENGTH_SHORT).show();
                } else {
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
//kin
                //                Toast.makeText(BaseActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return questionsRelatedArrayList;
    }

    /*public void getBetResult(List<QuestionsResult> questionsResultList) {

        API.matchList().getmatchlistdetails().enqueue(new retrofit2.Callback<List<MatchListDetail>>() {
            @Override
            public void onResponse(@NonNull Call<List<MatchListDetail>> call, @NonNull Response<List<MatchListDetail>> response) {
                matchListDetail = response.body();
                if (matchListDetail != null && matchListDetail.size() > 0 && questionsResultList != null && questionsResultList.size() > 0 ) {
                    for (int i = 0; i < matchListDetail.size(); i++) {
                        for (int j = 0; j < questionsResultList.size(); j++) {
//                            Toast.makeText(BaseActivity.this, "data : "+getQuestionsRelatedDetail(matchListDetail.get(i).getMatchID()).size(), Toast.LENGTH_SHORT).show();
                            if (questionsResultList.get(j).getMatchID().equals(matchListDetail.get(i).getMatchID())
                                    && questionsResultList.get(j).getOddtime().equals(questionsRelatedArrayList.get(0).getOddtime())
                                    && questionsResultList.get(j).getQueNumber().equals("1")) {
                                if (questionsResultList.get(j).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK10())) {
                                    prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(j).getBetCoins()) * Integer.parseInt(questionsResultList.get(j).getResultNumber())));
                                    Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                } else {
                                    prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(j).getBetCoins()));
                                    Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                }
                                break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<MatchListDetail>> call, Throwable t) {
            }
        });
    }
*/

    public void getBetResult(List<QuestionsResult> questionsResultList) {
        API.matchList().getmatchlistdetails().enqueue(new retrofit2.Callback<List<MatchListDetail>>() {
            @Override
            public void onResponse(@NonNull Call<List<MatchListDetail>> call, @NonNull Response<List<MatchListDetail>> response) {
                matchListDetail = response.body();
                if (matchListDetail != null && matchListDetail.size() > 0 && questionsResultList != null && questionsResultList.size() > 0) {

                    for (int i = 0; i < matchListDetail.size(); i++) {
                        for (int j = 0; j < questionsResultList.size(); j++) {
                            int finalJ = j;
                            int finalI = i;
                            API.graphlist().getquestionsrelateddetails(matchListDetail.get(i).getMatchID()).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
                                @Override
                                public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                                    CommonTime.JsonDetails jsonDetails = response.body();
                                    assert jsonDetails != null;
                                    questionsRelatedArrayList = jsonDetails.getQuestionsRelated();
                                    if (questionsRelatedArrayList != null && questionsRelatedArrayList.size() > 0) {
//                                        Toast.makeText(BaseActivity.this, "" + questionsResultList.get(finalJ).getMatchID(), Toast.LENGTH_SHORT).show();
                                        if (questionsResultList.get(finalJ).getMatchID().equals(matchListDetail.get(finalI).getMatchID())
                                                && questionsResultList.get(finalJ).getOddtime().equals(questionsRelatedArrayList.get(0).getOddtime())
                                                && questionsResultList.get(finalJ).getQueNumber().equals("1")) {

//                                            Log.e("mqq:", "onRes: "+questionsRelatedArrayList.get(5).getTeamK10());
                                           /* if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK10())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK10(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK10(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK20())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK20(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK20(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK30())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK30(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK30(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK40())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK40(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK40(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK50())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK50(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK50(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK60())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK60(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK60(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK70())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK70(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK70(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK80())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK80(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK80(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK90())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK90(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK90(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }

                                            if (questionsResultList.get(finalJ).getResultNumber().equals(questionsRelatedArrayList.get(5).getTeamK100())) {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK100(), "won");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() + (Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()) * Integer.parseInt(questionsResultList.get(finalJ).getResultNumber())));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                dbHandler.updateBetResult(matchListDetail.get(finalI).getMatchID(), questionsRelatedArrayList.get(0).getOddtime(), "1", questionsRelatedArrayList.get(5).getTeamK100(), "lose");
                                                prefManager.setTotalCoins(prefManager.getTotalCoins() - Integer.parseInt(questionsResultList.get(finalJ).getBetCoins()));
                                                Toast.makeText(getApplicationContext(), "" + prefManager.getTotalCoins(), Toast.LENGTH_SHORT).show();
                                            }*/
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                                //kin
                                    //    Toast.makeText(BaseActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<MatchListDetail>> call, Throwable t) {
            }
        });
    }

    public static void showProgressDialog(Context mContext) {
        dismissDialog();
        progressDialog = new ProgressDialog(mContext, R.style.progressDialog_theme);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void dismissDialog() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

}