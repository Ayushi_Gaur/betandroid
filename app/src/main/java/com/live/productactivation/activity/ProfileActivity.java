package com.live.productactivation.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.DBHandler;
import com.live.productactivation.common.PrefManager;
import com.mikhaellopez.circularimageview.CircularImageView;

public class ProfileActivity extends BaseActivity {

    private TextView tv_totalCoins, tv_perDayCoins, tv_username;
    private TextView tv_game1, tv_oddtimeName1, tv_coins1;
    private TextView tv_game2, tv_oddtimeName2, tv_coins2;
    private TextView tv_game3, tv_oddtimeName3, tv_coins3;
    private CircularImageView iv_profile;
    private PrefManager prefManager;
    private DBHandler dbHandler;
    private ImageView imgbg;

    private ScaleGestureDetector mScaleGestureDetector;
    private float mScaleFactor = 1.0f;
    private LinearLayout ll;


    private float mScale = 1f;
    GestureDetector gestureDetector;
    private ScrollView layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        //   ll= findViewById(R.id.ll);
//        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        layout = findViewById(R.id.scrollView);

/*

        gestureDetector = new GestureDetector(this, new GestureListener());
        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                // firstly we will get the scale factor by which we want to zoom
                float scale = 1 - detector.getScaleFactor();
                float prevScale = mScale;
                mScale += scale;

                // we can maximise our focus to 10f only
                if (mScale > 1.2f)
                    mScale = 1.2f;

                ScaleAnimation scaleAnimation = new ScaleAnimation(1f / prevScale, 1f / mScale, 1f / prevScale, 1f / mScale, detector.getFocusX(), detector.getFocusY());

                // duration of animation will be 0.It will not change by self after that
                scaleAnimation.setDuration(0);

                scaleAnimation.setFillAfter(true);

                // initialising the scrollview


                // we are setting it as animation
                layout.startAnimation(scaleAnimation);
                return true;
            }
        });
*/

        prefManager = new PrefManager(this);
        dbHandler = new DBHandler(this);

        tv_totalCoins = findViewById(R.id.tv_totalCoins);
        tv_perDayCoins = findViewById(R.id.tv_perDayCoins);
        tv_game1 = findViewById(R.id.tv_game1);
        tv_oddtimeName1 = findViewById(R.id.tv_oddtimeName1);
        tv_coins1 = findViewById(R.id.tv_coins1);
        tv_game2 = findViewById(R.id.tv_game2);
        tv_oddtimeName2 = findViewById(R.id.tv_oddtimeName2);
        tv_coins2 = findViewById(R.id.tv_coins2);
        tv_game3 = findViewById(R.id.tv_game3);
        tv_oddtimeName3 = findViewById(R.id.tv_oddtimeName3);
        tv_coins3 = findViewById(R.id.tv_coins3);
        iv_profile = findViewById(R.id.iv_profile);
        tv_username = findViewById(R.id.tv_username);

        Glide.with(this).load(prefManager.getUserPhoto()).placeholder(R.drawable.ic_profile_img).into(iv_profile);
        tv_username.setText(prefManager.getUserName());
        tv_perDayCoins.setText(String.valueOf(prefManager.getPerDayCoin()));

        for (int i = 0; i < dbHandler.getBettingDetails().size(); i++) {
            if (i >= 3) {
                break;
            } else if (i == 0) {
                tv_game1.setText("" + dbHandler.getBettingDetails().get(0).getGameName());
                tv_oddtimeName1.setText(dbHandler.getBettingDetails().get(0).getOddtime() + " min, " + dbHandler.getBettingDetails().get(0).getNetName() + ", " + dbHandler.getBettingDetails().get(0).getResultName());
                tv_coins1.setText("" + dbHandler.getBettingDetails().get(0).getBetCoins() + " coins");
            } else if (i == 1) {
                tv_game2.setText("" + dbHandler.getBettingDetails().get(1).getGameName());
                tv_oddtimeName2.setText(dbHandler.getBettingDetails().get(1).getOddtime() + " min, " + dbHandler.getBettingDetails().get(1).getNetName() + ", " + dbHandler.getBettingDetails().get(1).getResultName());
                tv_coins2.setText("" + dbHandler.getBettingDetails().get(1).getBetCoins() + " coins");
            } else if (i == 2) {
                tv_game3.setText("" + dbHandler.getBettingDetails().get(2).getGameName());
                tv_oddtimeName3.setText(dbHandler.getBettingDetails().get(2).getOddtime() + " min, " + dbHandler.getBettingDetails().get(2).getNetName() + ", " + dbHandler.getBettingDetails().get(2).getResultName());
                tv_coins3.setText("" + dbHandler.getBettingDetails().get(2).getBetCoins() + " coins");
            }
        }
    }

/*    public boolean onTouchEvent(MotionEvent motionEvent) {
        mScaleGestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector scaleGestureDetector){
            mScaleFactor *= scaleGestureDetector.getScaleFactor();
            mScaleFactor = Math.max(1f, Math.min(mScaleFactor, 1.2f));
            ll.setScaleX(mScaleFactor);
            ll.setScaleY(mScaleFactor);
            return true;

        }
    }*/

   /* @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        super.dispatchTouchEvent(event);

        // special types of touch screen events such as pinch ,
        // double tap, scrolls , long presses and flinch,
        // onTouch event is called if found any of these
        mScaleGestureDetector.onTouchEvent(event);
        gestureDetector.onTouchEvent(event);
        return gestureDetector.onTouchEvent(event);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            return true;
        }
    }
*/

}