package com.live.productactivation.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.PrefManager;
import com.live.productactivation.model.LoginDetails;
import com.live.productactivation.rest.LoginAPI;

import retrofit2.Call;
import retrofit2.Response;

public class SteamActivity extends BaseActivity {

    final String REALM_PARAM = "ProductActivation";
    PrefManager prefManager;
    private ImageView imgbg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steam);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        prefManager = new PrefManager(this);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        final WebView webView = new WebView(this);
        webView.getSettings().setJavaScriptEnabled(true);

        setContentView(webView);

        // Constructing openid url request
        String url = "https://steamcommunity.com/openid/login?" +
                "openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&" +
                "openid.identity=http://specs.openid.net/auth/2.0/identifier_select&" +
                "openid.mode=checkid_setup&" +
                "openid.ns=http://specs.openid.net/auth/2.0&" +
                "openid.realm=https://" + REALM_PARAM + "&" +
                "openid.return_to=https://" + REALM_PARAM + "/signin/";

        webView.loadUrl(url);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url,
                                      Bitmap favicon) {

                //checks the url being loaded
                setTitle(url);
                Uri Url = Uri.parse(url);

                if (Url.getAuthority().equals(REALM_PARAM.toLowerCase())) {
                    // That means that authentication is finished and the url contains user's id.
                    webView.stopLoading();

                    // Extracts user id.
                    Uri userAccountUrl = Uri.parse(Url.getQueryParameter("openid.identity"));
                    String userId = userAccountUrl.getLastPathSegment();
                    loginResponse(userId);
                    // Do whatever you want with the user's steam id

                }
            }
        });
    }

    public void loginResponse(String givenName) {
        LoginAPI.login().getlogindetails(givenName.trim(),null).enqueue(new retrofit2.Callback<LoginDetails>() {
            @Override
            public void onResponse(@NonNull Call<LoginDetails> call, @NonNull Response<LoginDetails> response) {
                LoginDetails loginDetails = response.body();
                assert loginDetails != null;
                if (loginDetails.getSuccess() == true) {
                  //kin
//                    Toast.makeText(SteamActivity.this, loginDetails.getMessage(), Toast.LENGTH_SHORT).show();
                    prefManager.setUserName(givenName);
                    prefManager.setUserToken(loginDetails.getToken());
                    startActivity(new Intent(SteamActivity.this, MainActivity.class));
                }
            }

            @Override
            public void onFailure(Call<LoginDetails> call, Throwable t) {
//kin
                //                Toast.makeText(SteamActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}