package com.live.productactivation.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.JsonObject;
import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.DBHandler;
import com.live.productactivation.common.PrefManager;
import com.live.productactivation.model.CommonTime;
import com.live.productactivation.model.GetTopUsers;
import com.live.productactivation.model.HeroIDAndName;
import com.live.productactivation.model.ImageModel;
import com.live.productactivation.model.MatchListDetail;
import com.live.productactivation.model.QuestionsRelated;
import com.live.productactivation.model.QuestionsResult;
import com.live.productactivation.rest.API;
import com.live.productactivation.rest.LoginAPI;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

public class BettingDetailsActivity extends BaseActivity {

    String game, duration, spectators, matchid;
    private TextView tv_game, tv_coins, tv_time, tv_watchAds, tv_no_data, tv_last_1_bets, tv_last_2_bets, tv_last_3_bets;
    private LinearLayout ll_que_bet, ll_game_details;
    LottieAnimationView lottie_progress;
    private ImageView imgbg;

    PrefManager prefManager;
    DBHandler dbHandler;
    List<QuestionsRelated> questionsRelatedArrayList = new ArrayList<>();
    List<ImageModel> imageModelList = new ArrayList<>();
    List<HeroIDAndName> heroIDAndNameList = new ArrayList<>();
    List<QuestionsResult> questionsResultList = new ArrayList<>();
    List<MatchListDetail> matchListDetail = new ArrayList<>();

    double betWin, totalCoins;
    //1
    private FrameLayout fl_radiant1, fl_dire1, fl_bet1;
    private EditText et_bet_amount1;
    private TextView tv_listName1, tv_radiant1, tv_dire1;
    private ImageView iv_r_alpha1, iv_d_alpha1;
    String result1, res1;
    int oddTime;
    //2
    private FrameLayout fl_radiant2, fl_dire2, fl_bet2;
    private EditText et_bet_amount2;
    private TextView tv_listName2, tv_radiant2, tv_dire2;
    private ImageView iv_r_alpha2, iv_d_alpha2;
    String result2, res2;

    //3
    private FrameLayout fl_bet3;
    private EditText et_bet_amount3;
    private TextView tv_listName3, tv_option31, tv_option32, tv_option33, tv_option34, tv_31, tv_32, tv_33, tv_34;
    private ImageView iv_option31, iv_option32, iv_option33, iv_option34;
    private LinearLayout ll_option31, ll_option32, ll_option33, ll_option34;
    String result3, res3;
    //4
    private FrameLayout fl_bet4;
    private EditText et_bet_amount4;
    private TextView tv_listName4, tv_option41, tv_option42, tv_option43, tv_option44, tv_41, tv_42, tv_43, tv_44;
    private ImageView iv_option41, iv_option42, iv_option43, iv_option44;
    private LinearLayout ll_option41, ll_option42, ll_option43, ll_option44;
    String result4, res4;

    double s1, s2, multi;

    boolean isSelect1 = false, isSelect2 = false, isSelect3 = false, isSelect4 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_betting_details);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);


        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                Toast.makeText(getApplicationContext(), " sucesfull ", Toast.LENGTH_SHORT).show();
            }
        });


        prefManager = new PrefManager(this);
        dbHandler = new DBHandler(this);

        Intent intent = getIntent();
        game = intent.getStringExtra("game");
        duration = intent.getStringExtra("duration");
        spectators = intent.getStringExtra("spectators");
        matchid = intent.getStringExtra("matchid");

        tv_game = findViewById(R.id.tv_game);
        tv_time = findViewById(R.id.tv_time);
        tv_coins = findViewById(R.id.tv_coins);
        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        ll_que_bet = findViewById(R.id.ll_que_bet);
        ll_game_details = findViewById(R.id.ll_game_details);
        tv_no_data = findViewById(R.id.tv_no_data);
        lottie_progress = findViewById(R.id.lottie_progress);
        tv_watchAds = findViewById(R.id.tv_watchAds);
        tv_last_1_bets = findViewById(R.id.tv_last_1_bets);
        tv_last_2_bets = findViewById(R.id.tv_last_2_bets);
        tv_last_3_bets = findViewById(R.id.tv_last_3_bets);

        //que 1
        tv_listName1 = findViewById(R.id.tv_listName1);
        fl_radiant1 = findViewById(R.id.fl_radiant1);
        fl_dire1 = findViewById(R.id.fl_dire1);
        fl_bet1 = findViewById(R.id.fl_bet1);
        et_bet_amount1 = findViewById(R.id.et_bet_amount1);
        tv_radiant1 = findViewById(R.id.tv_radiant1);
        tv_dire1 = findViewById(R.id.tv_dire1);
        iv_r_alpha1 = findViewById(R.id.iv_r_alpha1);
        iv_d_alpha1 = findViewById(R.id.iv_d_alpha1);

        //que 2
        tv_listName2 = findViewById(R.id.tv_listName2);
        fl_radiant2 = findViewById(R.id.fl_radiant2);
        fl_dire2 = findViewById(R.id.fl_dire2);
        fl_bet2 = findViewById(R.id.fl_bet2);
        et_bet_amount2 = findViewById(R.id.et_bet_amount2);
        tv_radiant2 = findViewById(R.id.tv_radiant2);
        tv_dire2 = findViewById(R.id.tv_dire2);
        iv_r_alpha2 = findViewById(R.id.iv_r_alpha2);
        iv_d_alpha2 = findViewById(R.id.iv_d_alpha2);

        //que 3
        tv_listName3 = findViewById(R.id.tv_listName3);
        tv_option31 = findViewById(R.id.tv_option31);
        tv_option32 = findViewById(R.id.tv_option32);
        tv_option33 = findViewById(R.id.tv_option33);
        tv_option34 = findViewById(R.id.tv_option34);
        iv_option31 = findViewById(R.id.iv_option31);
        iv_option32 = findViewById(R.id.iv_option32);
        iv_option33 = findViewById(R.id.iv_option33);
        iv_option34 = findViewById(R.id.iv_option34);
        ll_option31 = findViewById(R.id.ll_option31);
        ll_option32 = findViewById(R.id.ll_option32);
        ll_option33 = findViewById(R.id.ll_option33);
        ll_option34 = findViewById(R.id.ll_option34);
        tv_31 = findViewById(R.id.tv_31);
        tv_32 = findViewById(R.id.tv_32);
        tv_33 = findViewById(R.id.tv_33);
        tv_34 = findViewById(R.id.tv_34);
        fl_bet3 = findViewById(R.id.fl_bet3);
        et_bet_amount3 = findViewById(R.id.et_bet_amount3);

        //que 4
        tv_listName4 = findViewById(R.id.tv_listName4);
        tv_option41 = findViewById(R.id.tv_option41);
        tv_option42 = findViewById(R.id.tv_option42);
        tv_option43 = findViewById(R.id.tv_option43);
        tv_option44 = findViewById(R.id.tv_option44);
        iv_option41 = findViewById(R.id.iv_option41);
        iv_option42 = findViewById(R.id.iv_option42);
        iv_option43 = findViewById(R.id.iv_option43);
        iv_option44 = findViewById(R.id.iv_option44);
        ll_option41 = findViewById(R.id.ll_option41);
        ll_option42 = findViewById(R.id.ll_option42);
        ll_option43 = findViewById(R.id.ll_option43);
        ll_option44 = findViewById(R.id.ll_option44);
        tv_41 = findViewById(R.id.tv_41);
        tv_42 = findViewById(R.id.tv_42);
        tv_43 = findViewById(R.id.tv_43);
        tv_44 = findViewById(R.id.tv_44);
        fl_bet4 = findViewById(R.id.fl_bet4);
        et_bet_amount4 = findViewById(R.id.et_bet_amount4);

        tv_game.setSelected(true);
        tv_last_1_bets.setSelected(true);
        tv_last_2_bets.setSelected(true);
        tv_last_3_bets.setSelected(true);

        getQuestionsRelatedDetails(matchid);
        questionsResultList = dbHandler.getAllBetResult();
        getBetResult(questionsResultList);
        tv_coins.setText(String.valueOf(prefManager.getPerDayCoin()));
        tv_game.setText(game);

        CountDownTimer newtimer = new CountDownTimer(1000000000, 1000) {
            public void onTick(long millisUntilFinished) {
                tv_time.setText(new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date()));
            }

            public void onFinish() {
            }
        };
        newtimer.start();

        for (int i = 0; i < dbHandler.getBettingDetails().size(); i++) {
            if (i >= 3) {
                break;
            } else if (i == 0) {
                tv_last_1_bets.setText("" + dbHandler.getBettingDetails().get(0).getGameName() + ", " + dbHandler.getBettingDetails().get(0).getOddtime() + " min, " + dbHandler.getBettingDetails().get(0).getNetName() + ", " + dbHandler.getBettingDetails().get(0).getResultName() + ", " + dbHandler.getBettingDetails().get(0).getBetCoins() + " coins");
            } else if (i == 1) {
                tv_last_2_bets.setText("" + dbHandler.getBettingDetails().get(1).getGameName() + ", " + dbHandler.getBettingDetails().get(1).getOddtime() + " min, " + dbHandler.getBettingDetails().get(1).getNetName() + ", " + dbHandler.getBettingDetails().get(1).getResultName() + ", " + dbHandler.getBettingDetails().get(1).getBetCoins() + " coins");
            } else if (i == 2) {
                tv_last_3_bets.setText("" + dbHandler.getBettingDetails().get(2).getGameName() + ", " + dbHandler.getBettingDetails().get(2).getOddtime() + " min, " + dbHandler.getBettingDetails().get(2).getNetName() + ", " + dbHandler.getBettingDetails().get(2).getResultName() + ", " + dbHandler.getBettingDetails().get(2).getBetCoins() + " coins");
            }
        }

        tv_watchAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //kin
//                Toast.makeText(BettingDetailsActivity.this, "Watch Ads", Toast.LENGTH_SHORT).show();
            }
        });

        fl_radiant1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelect1 = true;
                iv_d_alpha1.setVisibility(View.VISIBLE);
                iv_r_alpha1.setVisibility(View.GONE);
                res1 = tv_radiant1.getText().toString();
                result1 = "Radiant";
            }
        });

        fl_dire1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelect1 = true;
                iv_d_alpha1.setVisibility(View.GONE);
                iv_r_alpha1.setVisibility(View.VISIBLE);
                res1 = tv_dire1.getText().toString();
                result1 = "Dire";
            }
        });

        fl_bet1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_bet_amount1.getText().toString().isEmpty()) {
                   //kin
//                    Toast.makeText(BettingDetailsActivity.this, "Enter Coin", Toast.LENGTH_SHORT).show();
                } else {
                    if (isSelect1 == true) {
                        new AlertDialog.Builder(BettingDetailsActivity.this)
                                .setTitle("Place a bet")
                                .setMessage("Are you sure you want to place a bet?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getInsertBet(prefManager.getUserToken(), result1, String.valueOf(res1), et_bet_amount1.getText().toString());
                                        //questionsResultList.add(new QuestionsResult(String.valueOf(matchid), String.valueOf(odd1), "1", String.valueOf(res1), result1, et_bet_amount1.getText().toString()));
                                        dbHandler.insertBetResult(String.valueOf(matchid), String.valueOf(oddTime), "1", String.valueOf(res1), result1, et_bet_amount1.getText().toString(), "pending");
                                        dbHandler.insertBettingDetails(game, String.valueOf(oddTime), "Net", result1, et_bet_amount1.getText().toString());
                                        // Toast.makeText(BettingDetailsActivity.this, ""+dbHandler.getBettingDetails().size(), Toast.LENGTH_SHORT).show();
                                        Last3Bets();
                                        et_bet_amount1.setText("");
                                        isSelect1 = false;
                                    }
                                })

                                .setNegativeButton("No", null)
                                .show();



                    } else {
                        //kin
//                        Toast.makeText(BettingDetailsActivity.this, "Select one bet option", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        fl_radiant2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelect2 = true;
                iv_d_alpha2.setVisibility(View.VISIBLE);
                iv_r_alpha2.setVisibility(View.GONE);
                res2 = tv_radiant2.getText().toString();
                result2 = "Radiant";
            }
        });

        fl_dire2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelect2 = true;
                iv_d_alpha2.setVisibility(View.GONE);
                iv_r_alpha2.setVisibility(View.VISIBLE);
                res2 = tv_dire2.getText().toString();
                result2 = "Dire";
            }
        });

        fl_bet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_bet_amount2.getText().toString().isEmpty()) {
                    //kin
//                    Toast.makeText(BettingDetailsActivity.this, "Enter Coin", Toast.LENGTH_SHORT).show();
                } else {
                    if (isSelect2 == true) {
                        new AlertDialog.Builder(BettingDetailsActivity.this)
                                .setTitle("Place a bet")
                                .setMessage("Are you sure you want to place a bet?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getInsertBet(prefManager.getUserToken(), result2, String.valueOf(res2), et_bet_amount2.getText().toString());
                                        dbHandler.insertBetResult(String.valueOf(matchid), String.valueOf(oddTime), "2", String.valueOf(res2), result2, et_bet_amount2.getText().toString(), "pending");
                                        dbHandler.insertBettingDetails(game, String.valueOf(oddTime), "Net", result2, et_bet_amount2.getText().toString());
                                        Last3Bets();
                                        et_bet_amount2.setText("");
                                        isSelect2 = false;
                                    }
                                })

                                .setNegativeButton("No", null)
                                .show();

                    } else {
                        //kin
//                        Toast.makeText(BettingDetailsActivity.this, "Select one bet option", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ll_option31.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect3 = true;
                ll_option31.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option32.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option33.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option34.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res3 = tv_option31.getText().toString();
                result3 = tv_31.getText().toString();
            }
        });

        ll_option32.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect3 = true;
                ll_option32.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option31.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option33.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option34.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res3 = tv_option32.getText().toString();
                result3 = tv_32.getText().toString();
            }
        });

        ll_option33.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect3 = true;
                ll_option33.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option31.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option32.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option34.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res3 = tv_option33.getText().toString();
                result3 = tv_33.getText().toString();
            }
        });

        ll_option34.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect3 = true;
                ll_option34.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option31.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option32.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option33.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res3 = tv_option34.getText().toString();
                result3 = tv_34.getText().toString();
            }
        });

        fl_bet3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_bet_amount3.getText().toString().isEmpty()) {
                    //kin
//                    Toast.makeText(BettingDetailsActivity.this, "Enter Coin", Toast.LENGTH_SHORT).show();
                } else {
                    if (isSelect3 == true) {
                        new AlertDialog.Builder(BettingDetailsActivity.this)
                                .setTitle("Place a bet")
                                .setMessage("Are you sure you want to place a bet?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getInsertBet(prefManager.getUserToken(), result3, String.valueOf(res3), et_bet_amount3.getText().toString());
                                        dbHandler.insertBetResult(String.valueOf(matchid), String.valueOf(oddTime), "3", String.valueOf(res3), result3, et_bet_amount3.getText().toString(), "pending");
                                        dbHandler.insertBettingDetails(game, String.valueOf(oddTime), "Net", result3, et_bet_amount3.getText().toString());
                                        Last3Bets();
                                        et_bet_amount3.setText("");
                                        isSelect3 = false;
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton("No", null)
                                .show();


                    } else {
                        //kin
//                        Toast.makeText(BettingDetailsActivity.this, "Select one bet option", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ll_option41.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect4 = true;
                ll_option41.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option42.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option43.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option44.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res4 = tv_option41.getText().toString();
                result4 = tv_41.getText().toString();
            }
        });

        ll_option42.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect4 = true;
                ll_option42.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option41.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option43.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option44.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res4 = tv_option42.getText().toString();
                result4 = tv_42.getText().toString();
            }
        });

        ll_option43.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect4 = true;
                ll_option43.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option41.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option42.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option44.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res4 = tv_option43.getText().toString();
                result4 = tv_43.getText().toString();
            }
        });

        ll_option44.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                isSelect4 = true;
                ll_option44.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.transparent)));
                ll_option41.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option42.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                ll_option43.setForeground(new ColorDrawable(ContextCompat.getColor(BettingDetailsActivity.this, R.color.black_alpha)));
                res4 = tv_option44.getText().toString();
                result4 = tv_44.getText().toString();
            }
        });

        fl_bet4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_bet_amount4.getText().toString().isEmpty()) {
                    //kin
//                    Toast.makeText(BettingDetailsActivity.this, "Enter Coin", Toast.LENGTH_SHORT).show();
                } else {
                    if (isSelect4 == true) {
                        new AlertDialog.Builder(BettingDetailsActivity.this)
                                .setTitle("Place a bet")
                                .setMessage("Are you sure you want to place a bet?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        getInsertBet(prefManager.getUserToken(), result4, String.valueOf(res4), et_bet_amount4.getText().toString());
                                        dbHandler.insertBetResult(String.valueOf(matchid), String.valueOf(oddTime), "4", String.valueOf(res4), result4, et_bet_amount4.getText().toString(), "pending");
                                        dbHandler.insertBettingDetails(game, String.valueOf(oddTime), "Net", result4, et_bet_amount4.getText().toString());
                                        Last3Bets();
                                        et_bet_amount4.setText("");
                                        isSelect4 = false;
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton("No", null)
                                .show();


                    } else {
                        //kin
//                        Toast.makeText(BettingDetailsActivity.this, "Select one bet option", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void getQuestionsRelatedDetails(String matchid) {
        lottie_progress.setVisibility(View.VISIBLE);
        ll_game_details.setVisibility(View.GONE);
        tv_no_data.setVisibility(View.GONE);
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                questionsRelatedArrayList = jsonDetails.getQuestionsRelated();
                imageModelList = jsonDetails.getImagesForBox();
                heroIDAndNameList = jsonDetails.getHeroIDAndNames();

                lottie_progress.setVisibility(View.GONE);
                ll_game_details.setVisibility(View.VISIBLE);
                tv_no_data.setVisibility(View.GONE);
                if (questionsRelatedArrayList != null && questionsRelatedArrayList.size() > 0 && imageModelList != null && imageModelList.size() > 0 && heroIDAndNameList != null && heroIDAndNameList.size() > 0) {
                    //que 1
                    oddTime = Integer.parseInt(questionsRelatedArrayList.get(0).getOddtime());
                    tv_listName1.setText("Which team will have the highest Networth at " + oddTime + "th min:");
                    tv_radiant1.setText("" + questionsRelatedArrayList.get(1).getRnetworthodds());
                    tv_dire1.setText("" + questionsRelatedArrayList.get(1).getDnetworthodds());

                    //que 2
                    tv_listName2.setText("Which team will have the highest Kills at " + questionsRelatedArrayList.get(0).getOddtime() + "th min:");
                    tv_radiant2.setText("" + questionsRelatedArrayList.get(2).getRkodds());
                    tv_dire2.setText("" + questionsRelatedArrayList.get(2).getDkodds());

                    //que 3
                    tv_listName3.setText("Which Hero will have the highest Networth at " + questionsRelatedArrayList.get(0).getOddtime() + "th min:");

                    List<Double> list3 = new ArrayList<>();
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero1Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero2Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero3Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero4Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero5Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero6Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero7Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero8Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero9Netodds()));
                    list3.add(Double.valueOf(questionsRelatedArrayList.get(3).getHero10Netodds()));

                    Collections.sort(list3, new Comparator<Double>() {
                        @Override
                        public int compare(Double o1, Double o2) {
                            return o1.compareTo(o2);
                        }
                    });

                    tv_option31.setText(list3.get(0).toString());
                    tv_option32.setText(list3.get(1).toString());
                    tv_option33.setText(list3.get(2).toString());
                    tv_option34.setText(list3.get(3).toString());

                    for (int i = 0; i < heroIDAndNameList.size(); i++) {

                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(0).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img1).into(iv_option41);
                            tv_31.setText(heroIDAndNameList.get(i).getName());
                        }

                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(1).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img2).into(iv_option42);
                            tv_32.setText(heroIDAndNameList.get(i).getName());
                        }

                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(2).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img3).into(iv_option43);
                            tv_33.setText(heroIDAndNameList.get(i).getName());
                        }

                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(3).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img4).into(iv_option44);
                            tv_34.setText(heroIDAndNameList.get(i).getName());
                        }

                    }

                    //que 4
                    tv_listName4.setText("Which Hero will have the highest Kills at " + questionsRelatedArrayList.get(0).getOddtime() + "th min:");
                    List<Double> list4 = new ArrayList<>();
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero1Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero2Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero3Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero4Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero5Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero6Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero7Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero8Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero9Killodds()));
                    list4.add(Double.valueOf(questionsRelatedArrayList.get(4).getHero10Killodds()));
                    Collections.sort(list4, new Comparator<Double>() {
                        @Override
                        public int compare(Double o1, Double o2) {
                            return o1.compareTo(o2);
                        }
                    });
                    tv_option41.setText(list4.get(0).toString());
                    tv_option42.setText(list4.get(1).toString());
                    tv_option43.setText(list4.get(2).toString());
                    tv_option44.setText(list4.get(3).toString());

                    for (int i = 0; i < heroIDAndNameList.size(); i++) {
                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(0).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img1).into(iv_option31);
                            tv_41.setText(heroIDAndNameList.get(i).getName());
                        }
                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(1).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img2).into(iv_option32);
                            tv_42.setText(heroIDAndNameList.get(i).getName());
                        }
                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(2).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img3).into(iv_option33);
                            tv_43.setText(heroIDAndNameList.get(i).getName());
                        }
                        if (heroIDAndNameList.get(i).getId().equals(imageModelList.get(3).getImg())) {
                            Glide.with(BettingDetailsActivity.this).load(heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img4).into(iv_option34);
                            tv_44.setText(heroIDAndNameList.get(i).getName());
                        }
                    }

                } else {
                    lottie_progress.setVisibility(View.GONE);
                    ll_game_details.setVisibility(View.GONE);
                    tv_no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(BettingDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Last3Bets() {
        for (int i = 0; i < dbHandler.getBettingDetails().size(); i++) {
            if (i >= 3) {
                break;
            } else if (i == 0) {
                tv_last_1_bets.setText("" + dbHandler.getBettingDetails().get(0).getGameName() + ", " + dbHandler.getBettingDetails().get(0).getOddtime() + " min, " + dbHandler.getBettingDetails().get(0).getNetName() + ", " + dbHandler.getBettingDetails().get(0).getResultName() + ", " + dbHandler.getBettingDetails().get(0).getBetCoins() + " coins");
            } else if (i == 1) {
                tv_last_2_bets.setText("" + dbHandler.getBettingDetails().get(1).getGameName() + ", " + dbHandler.getBettingDetails().get(1).getOddtime() + " min, " + dbHandler.getBettingDetails().get(1).getNetName() + ", " + dbHandler.getBettingDetails().get(1).getResultName() + ", " + dbHandler.getBettingDetails().get(1).getBetCoins() + " coins");
            } else if (i == 2) {
                tv_last_3_bets.setText("" + dbHandler.getBettingDetails().get(2).getGameName() + ", " + dbHandler.getBettingDetails().get(2).getOddtime() + " min, " + dbHandler.getBettingDetails().get(2).getNetName() + ", " + dbHandler.getBettingDetails().get(2).getResultName() + ", " + dbHandler.getBettingDetails().get(2).getBetCoins() + " coins");
            }
        }
    }

    private void getInsertBet(String token, String betName, String betType, String betAmount) {
        JsonObject paramObject = new JsonObject();

        try {
            paramObject.addProperty("BetName", betName);
            paramObject.addProperty("BetType", betType);
            paramObject.addProperty("BetAmount", betAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }

        LoginAPI.login().getInsertBet(token, paramObject).enqueue(new retrofit2.Callback<GetTopUsers>() {
            @Override
            public void onResponse(@NonNull Call<GetTopUsers> call, @NonNull Response<GetTopUsers> response) {
                GetTopUsers getTopUsers = response.body();
                Log.e("this", "onResponse: " + getTopUsers.getSuccess());
                assert getTopUsers != null;
                if (getTopUsers.getSuccess() == true) {
                    prefManager.setPerDayCoin(prefManager.getPerDayCoin() - Integer.parseInt(betAmount));
                    tv_coins.setText(String.valueOf(prefManager.getPerDayCoin()));
                  //kin
                  // Toast.makeText(BettingDetailsActivity.this, getTopUsers.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    //kin
//                    Toast.makeText(BettingDetailsActivity.this, getTopUsers.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetTopUsers> call, Throwable t) {
               //kin
               // Toast.makeText(BettingDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        tv_time.setText(DateFormat.format("kk:mm:ss 'Uhr', dd. MMM", new Date()));
    }
}