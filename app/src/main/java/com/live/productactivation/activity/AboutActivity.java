package com.live.productactivation.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;

public class AboutActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_graph_explanation, ll_Betting_instructions, ll_betting_rewards, ll_update_log, ll_anouncements;
    private ImageView iv_back;
    private ImageView imgbg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        ll_graph_explanation = findViewById(R.id.ll_graph_explanation);
        ll_Betting_instructions = findViewById(R.id.ll_Betting_instructions);
        ll_betting_rewards = findViewById(R.id.ll_betting_rewards);
        ll_update_log = findViewById(R.id.ll_update_log);
        ll_anouncements = findViewById(R.id.ll_anouncements);
        iv_back = findViewById(R.id.iv_back);

        ll_graph_explanation.setOnClickListener(this);
        ll_Betting_instructions.setOnClickListener(this);
        ll_betting_rewards.setOnClickListener(this);
        ll_update_log.setOnClickListener(this);
        ll_anouncements.setOnClickListener(this);
        iv_back.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.ll_graph_explanation:
                //kin
//                Toast.makeText(this, "Graph Explanation", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ll_Betting_instructions:
                //kin
//                Toast.makeText(this, "Betting Instructions", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ll_betting_rewards:
                //kin
//                Toast.makeText(this, "Betting Rewards", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ll_update_log:
                //kin
//                Toast.makeText(this, "Update Log", Toast.LENGTH_SHORT).show();
                break;
            case R.id.ll_anouncements:
                //kin
//                Toast.makeText(this, "Anouncements", Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}