package com.live.productactivation.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.PrefManager;
import com.live.productactivation.model.LoginDetails;
import com.live.productactivation.rest.LoginAPI;

import retrofit2.Call;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private LinearLayout ll_loginWithGoogle, ll_loginWithStream;
    PrefManager prefManager;
    private int RC_SIGN_IN = 1;
    private ImageView imgbg;


    final String REALM_PARAM = String.valueOf(R.string.app_name);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }


        prefManager = new PrefManager(this);

        ll_loginWithGoogle = findViewById(R.id.ll_loginWithGoogle);
        ll_loginWithStream = findViewById(R.id.ll_loginWithSteam);

        ll_loginWithGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(googleSignInClient.getSignInIntent(), RC_SIGN_IN);
            }
        });

        ll_loginWithStream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SteamActivity.class));
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                FirebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                //kin
//                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void FirebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential authCredential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    updateUI(user);
                } else {
                    //kin
                    // Toast.makeText(LoginActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                    updateUI(null);
                }
            }
        });
    }

    private void updateUI(FirebaseUser user) {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();
            loginResponse(personGivenName, String.valueOf(personPhoto));
        }
    }

    public void loginResponse(String givenName, String photo) {
        LoginAPI.login().getlogindetails(givenName.trim(), photo.trim()).enqueue(new retrofit2.Callback<LoginDetails>() {
            @Override
            public void onResponse(@NonNull Call<LoginDetails> call, @NonNull Response<LoginDetails> response) {
                LoginDetails loginDetails = response.body();
                assert loginDetails != null;
                if (loginDetails.getSuccess() == true) {

                    //kin
//                    Toast.makeText(LoginActivity.this, loginDetails.getMessage(), Toast.LENGTH_SHORT).show();
                    prefManager.setUserName(givenName);
                    prefManager.setUserPhoto(String.valueOf(photo));
                    prefManager.setUserToken(loginDetails.getToken());
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }
            }

            @Override
            public void onFailure(Call<LoginDetails> call, Throwable t) {
                //kin
//                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}