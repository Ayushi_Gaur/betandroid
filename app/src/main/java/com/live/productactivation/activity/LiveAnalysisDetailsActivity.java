package com.live.productactivation.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.live.mpchartlib.charts.HorizontalBarChart;
import com.live.mpchartlib.charts.LineChart;
import com.live.mpchartlib.components.Legend;
import com.live.mpchartlib.components.XAxis;
import com.live.mpchartlib.components.YAxis;
import com.live.mpchartlib.data.BarData;
import com.live.mpchartlib.data.BarDataSet;
import com.live.mpchartlib.data.BarEntry;
import com.live.mpchartlib.data.Entry;
import com.live.mpchartlib.data.LineData;
import com.live.mpchartlib.data.LineDataSet;
import com.live.mpchartlib.formatter.IndexAxisValueFormatter;
import com.live.productactivation.R;
import com.live.productactivation.adapter.StatisticsAdapter;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.CustomProgressbar;
import com.live.productactivation.common.Scheduler;
import com.live.productactivation.model.BarWin;
import com.live.productactivation.model.CommonTime;
import com.live.productactivation.model.DraftAdvantage;
import com.live.productactivation.model.HeroIDAndName;
import com.live.productactivation.model.LiveAdvantage;
import com.live.productactivation.model.Table1;
import com.live.productactivation.model.TeamFight;
import com.live.productactivation.model.TimeGraph1;
import com.live.productactivation.model.TimeGraph2;
import com.live.productactivation.model.TimeGraph3;
import com.live.productactivation.model.TotalAdvantage;
import com.live.productactivation.rest.API;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import retrofit2.Call;
import retrofit2.Response;

public class LiveAnalysisDetailsActivity extends BaseActivity implements View.OnClickListener {

    String game, duration, spectators, matchid;
    private TextView tv_game, tv_duration, tv_listName, tv_dropdown;
    private TextView tv_win_chance, tv_win_chance_timeline, tv_team_fight, tv_team_fight_timeline, tv_live_advantage, tv_live_advantage_timeline, tv_total_advantage, tv_draft_advantage, tv_statistics, tv_draft_suggestion;
    private ImageView iv_dropdownmenu, iv_back;
    private LinearLayout ll_dropdown, ll_rd;

    Scheduler mScheduler;

    //win chance :
    private HorizontalBarChart chart_winchance;
    List<BarEntry> radiantList = new ArrayList<>();
    List<BarEntry> direList = new ArrayList<>();
    String[] winChanceLabels = new String[]{"", "Win Chance"};
    private List<BarWin> winChanceList = new ArrayList<>();
    public float ry, dy;

    //win chance timeline :
    private LineChart chart_winchance_timeline;
    LineData ld_winchance_timeline = new LineData();

    //team fight :
    private HorizontalBarChart chart_teamfight;
    final String[] teamFightLabels = new String[]{"", "Aura", "Healing", "Initiation", "Durability", "R Dmg", "Nuke Dmg", "Disables"};
    private List<TeamFight> teamFightList = new ArrayList<>();
    public float disables_radiant, disables_dire, nuke_radiant, nuke_dire, r_radiant, r_dire, durability_radiant, durability_dire, initiation_radiant, initiation_dire, healing_radiant, healing_dire, aura_radiant, aura_dire;

    //team fight timeline :
    private LineChart chart_teamfight_timeline;
    LineData ld_teamfight_timeline = new LineData();

    //live advantage :
    private HorizontalBarChart chart_liveadvantage;
    final String[] liveAdvantageLabels = new String[]{"", "Split Pushing", "Defending", "Pushing", "Team Fight"};
    private List<LiveAdvantage> liveAdvantageList = new ArrayList<>();
    public float teamFight_Radiant, teamFight_Dire, pushing_Radiant, pushing_Dire, defending_Radiant, defending_Dire, splitPushing_Radiant, splitPushing_Dire;

    //live advantage timeline :
    private LineChart chart_liveadvantage_timeline;
    LineData ld_liveadvantage_timeline = new LineData();

    //total advantage :
    private HorizontalBarChart chart_total_advantage;
    final String[] labels = new String[]{"", "Live Advantage", "Draft Advantage"};
    private List<TotalAdvantage> totalAdvantageList = new ArrayList<>();
    public float draft_radiant, draft_dire, live_radiant, live_dire;

    //draft advantage :
    private HorizontalBarChart chart_draft_advantage;
    final String[] DraftAdvantageLabels = new String[]{"", "Counters", "Initiation", "Healing", "Mobility", "Durability", "R Dmg", "Split Pushing", "Defending", "Pushing", "Nukes", "Disables"};
    private List<DraftAdvantage> draftAdvantageList = new ArrayList<>();
    public float nukes_radiant, nukes_dire, pushing_radiant, pushing_dire, defending_radiant, defending_dire, splitPushing_radiant, splitPushing_dire, rDmg_radiant, rDmg_dire, mobility_radiant, mobility_dire, counters_radiant, counters_dire;

    //Statistics :
    private RecyclerView rv_statistics;
    private LottieAnimationView lottie_progress;
    private TextView tv_no_data;
    private LinearLayoutManager layoutManager;
    private StatisticsAdapter adapter;
    List<Table1> table1ArrayList = new ArrayList<>();
    public List<HeroIDAndName> heroIDAndNameList = new ArrayList<>();
    private RelativeLayout rl_statistics;
    private ImageView imgbg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liveanalysis_details);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }


        Intent intent = getIntent();
        game = intent.getStringExtra("game");
        duration = intent.getStringExtra("duration");
        spectators = intent.getStringExtra("spectators");
        matchid = intent.getStringExtra("matchid");

        tv_game = findViewById(R.id.tv_game);
        tv_duration = findViewById(R.id.tv_duration);
        iv_dropdownmenu = findViewById(R.id.iv_dropdownmenu);
        iv_back = findViewById(R.id.iv_back);
        tv_listName = findViewById(R.id.tv_listName);
        tv_dropdown = findViewById(R.id.tv_dropdown);
        ll_dropdown = findViewById(R.id.ll_dropdown);

        ll_rd = findViewById(R.id.ll_rd);
        rl_statistics = findViewById(R.id.rl_statistics);
        rv_statistics = findViewById(R.id.rv_statistics);
        lottie_progress = findViewById(R.id.lottie_progress);
        tv_no_data = findViewById(R.id.tv_no_data);

        tv_win_chance = findViewById(R.id.tv_win_chance);
        tv_win_chance_timeline = findViewById(R.id.tv_winchance_timeline);
        tv_team_fight = findViewById(R.id.tv_team_fight);
        tv_team_fight_timeline = findViewById(R.id.tv_team_fight_timeline);
        tv_live_advantage = findViewById(R.id.tv_live_advantage);
        tv_live_advantage_timeline = findViewById(R.id.tv_live_advantage_timeline);
        tv_total_advantage = findViewById(R.id.tv_total_advantage);
        tv_draft_advantage = findViewById(R.id.tv_draft_advantage);
        tv_statistics = findViewById(R.id.tv_statistics);
        tv_draft_suggestion = findViewById(R.id.tv_draft_suggestion);

        chart_winchance = findViewById(R.id.chart_winchance);
        chart_winchance_timeline = findViewById(R.id.chart_winchance_timeline);
        chart_teamfight = findViewById(R.id.chart_teamfight);
        chart_teamfight_timeline = findViewById(R.id.chart_teamfight_timeline);
        chart_liveadvantage = (HorizontalBarChart) findViewById(R.id.chart_liveadvantage);
        chart_liveadvantage_timeline = findViewById(R.id.chart_liveadvantage_timeline);
        chart_total_advantage = (HorizontalBarChart) findViewById(R.id.chart_total_advantage);
        chart_draft_advantage = (HorizontalBarChart) findViewById(R.id.chart_draft_advantage);

        tv_win_chance.setOnClickListener(this);
        tv_win_chance_timeline.setOnClickListener(this);
        tv_team_fight.setOnClickListener(this);
        tv_team_fight_timeline.setOnClickListener(this);
        tv_live_advantage.setOnClickListener(this);
        tv_live_advantage_timeline.setOnClickListener(this);
        tv_total_advantage.setOnClickListener(this);
        tv_draft_advantage.setOnClickListener(this);
        tv_statistics.setOnClickListener(this);
        tv_draft_suggestion.setOnClickListener(this);

        tv_game.setText(game);
        tv_duration.setText(duration);

        iv_dropdownmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             setTimer(matchid,10);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataShow();
            }
        });

        setTimer(matchid,1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_win_chance:
                tv_listName.setText("Win Chance");
                setTimer(matchid, 1);
                break;

            case R.id.tv_winchance_timeline:
                tv_listName.setText("Win Chance (Timeline)");
                setTimer(matchid, 2);
                break;

            case R.id.tv_team_fight:
                tv_listName.setText("Team Fight");
                setTimer(matchid, 3);
                break;

            case R.id.tv_team_fight_timeline:
                tv_listName.setText("Team Fight (Timeline)");
                setTimer(matchid, 4);
                break;

            case R.id.tv_live_advantage:
                tv_listName.setText("Live Advantage");
                setTimer(matchid, 5);
                break;

            case R.id.tv_live_advantage_timeline:
                tv_listName.setText("Live Advantage (Timeline)");
                setTimer(matchid, 6);
                break;

            case R.id.tv_total_advantage:
                tv_listName.setText("Total Advantage");
                setTimer(matchid, 7);
                break;

            case R.id.tv_draft_advantage:
                tv_listName.setText("Draft Advantage");
                setTimer(matchid, 8);
                break;

            case R.id.tv_statistics:
                tv_listName.setText("Statistics");
                setTimer(matchid, 9);
                break;

            case R.id.tv_draft_suggestion:

                getDataShow();
                break;

        }
    }

    private void getDataShow() {
        tv_listName.setVisibility(View.VISIBLE);
        tv_dropdown.setVisibility(View.GONE);
        ll_dropdown.setVisibility(View.GONE);
        iv_dropdownmenu.setVisibility(View.VISIBLE);
        iv_back.setVisibility(View.GONE);
    }

    public void getDropDropList(){
        tv_listName.setText("Select Graph");
        tv_listName.setVisibility(View.GONE);
        tv_dropdown.setVisibility(View.VISIBLE);
        ll_dropdown.setVisibility(View.VISIBLE);
        iv_dropdownmenu.setVisibility(View.GONE);
        iv_back.setVisibility(View.VISIBLE);

        chart_winchance.setVisibility(View.GONE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.GONE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.GONE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.GONE);
        chart_draft_advantage.setVisibility(View.GONE);
        rl_statistics.setVisibility(View.GONE);
        ll_rd.setVisibility(View.GONE);
    }

    public void setTimer(String matchid1, int num) {
        stopTimer();
        mScheduler = new Scheduler(15000);//every 15 sec
        mScheduler.setOnScheduleTimeListener(new Scheduler.OnScheduleTimeListener() {
            @Override
            public void onScheduleTime() {
                if (num == 1) {
                    getWinChanceData(matchid1);
                } else if (num == 2) {
                    getWinChanceTimeline(matchid1);
                } else if (num == 3) {
                    getTeamFightData(matchid1);
                } else if (num == 4) {
                    getTeamFightTimeline(matchid1);
                } else if (num == 5) {
                    getLiveAdvantageData(matchid1);
                } else if (num == 6) {
                    getLiveAdvantageTimeline(matchid1);
                } else if (num == 7) {
                    getTotalAdvantageData(matchid1);
                } else if (num == 8) {
                    getDraftAdvantageData(matchid1);
                } else if (num == 9) {
                    getStatisticsData(matchid1);
                }else if (num == 10){
                    getDropDropList();
                }
            }
        });
        mScheduler.startTimer();
    }

    private void stopTimer() {
        if (mScheduler != null && mScheduler.isTimerRunning()) {
            mScheduler.stopTimer();
            mScheduler = null;
        }
    }

    public void getWinChanceData(String matchid) {
//        CustomProgressbar.showProgressBar(LiveAnalysisDetailsActivity.this, false);

        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                winChanceList = jsonDetails.getBarWin();
                CustomProgressbar.hideProgressBar();
                if (winChanceList != null && winChanceList.size() > 0) {

                    if (winChanceList.get(0).getRwinchance().equals("")) {
                        ry = 0;
                    } else {
                        ry = Float.parseFloat(winChanceList.get(0).getRwinchance());
                    }

                    if (winChanceList.get(0).getDwinchance().equals("")) {
                        dy = 0;
                    } else {
                        dy = Float.parseFloat(winChanceList.get(0).getDwinchance());
                    }

                    createWinChanceChart(ry, ry + dy);
                } else {
//                    CustomProgressbar.hideProgressBar();
                    createWinChanceChart(0, 0);
                }

            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
//kin
                //                Toast.makeText(WinChanceActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createWinChanceChart(float ry, float dy) {//r(radient)->blue,d(dire)->red

        chart_winchance.clear();

        chart_winchance.setVisibility(View.VISIBLE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.GONE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.GONE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.GONE);
        chart_draft_advantage.setVisibility(View.GONE);
        rl_statistics.setVisibility(View.GONE);
        ll_rd.setVisibility(View.GONE);

        getDataShow();

        direList.add(new BarEntry(1, dy));
        radiantList.add(new BarEntry(1, ry));

        BarDataSet barDataSet2 = new BarDataSet(radiantList, "Radiant");
        barDataSet2.setColor(getResources().getColor(R.color.radiant_blue));
        BarDataSet barDataSet = new BarDataSet(direList, "Dire");
        barDataSet.setColor(getResources().getColor(R.color.dire_red));
        BarData barData = new BarData(barDataSet);

        barData.addDataSet(barDataSet2);
        barData.setValueTextColor(Color.TRANSPARENT);
        chart_winchance.setData(barData);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            barData.setBarWidth(0.2f);
        } else {
            barData.setBarWidth(0.5f);
        }

        chart_winchance.getAxisLeft().setAxisMaximum(100);
        chart_winchance.getAxisLeft().setAxisMinimum(0);
        chart_winchance.getAxisLeft().setEnabled(false);
        chart_winchance.getXAxis().setAxisMaximum(2);
        chart_winchance.getXAxis().setAxisMinimum(0);
        chart_winchance.getXAxis().setLabelCount(2, false);

        chart_winchance.getXAxis().setValueFormatter(new IndexAxisValueFormatter(winChanceLabels));
        chart_winchance.getDescription().setEnabled(false);
        chart_winchance.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart_winchance.getXAxis().setTextColor(Color.WHITE);
        chart_winchance.getAxisRight().setAxisMaximum(100);
        chart_winchance.getAxisRight().setAxisMinimum(0);
        chart_winchance.getAxisRight().setEnabled(true);
        chart_winchance.getAxisRight().setTextColor(Color.WHITE);
        chart_winchance.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        chart_winchance.getLegend().setTextColor(Color.WHITE);

    }

    private void setWinChanceTimelineData(int count, float range) {
        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(1, 99));
        values.add(new Entry(2, -99));

        LineDataSet set1;

        if (chart_winchance_timeline.getData() != null &&
                chart_winchance_timeline.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart_winchance_timeline.getData().getDataSetByIndex(0);
            chart_winchance_timeline.getData().notifyDataChanged();
            chart_winchance_timeline.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(false);
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(4f);
            set1.setColor(Color.TRANSPARENT);
            set1.setFillAlpha(0);
            set1.setDrawHorizontalHighlightIndicator(false);
            ld_winchance_timeline.addDataSet(set1);
            ld_winchance_timeline.setDrawValues(false);
        }
    }

    public void getWinChanceTimeline(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;
//            winChanceLabels=null;
                chart_winchance_timeline.clear();
//                ld_winchance_timeline.clearValues();

                chart_winchance.setVisibility(View.GONE);
                chart_winchance_timeline.setVisibility(View.VISIBLE);
                chart_teamfight.setVisibility(View.GONE);
                chart_teamfight_timeline.setVisibility(View.GONE);
                chart_liveadvantage.setVisibility(View.GONE);
                chart_liveadvantage_timeline.setVisibility(View.GONE);
                chart_total_advantage.setVisibility(View.GONE);
                chart_draft_advantage.setVisibility(View.GONE);
                rl_statistics.setVisibility(View.GONE);
                ll_rd.setVisibility(View.VISIBLE);

                getDataShow();

                chart_winchance_timeline.setViewPortOffsets(100, 50, 0, 50);

                chart_winchance_timeline.setTouchEnabled(true);
                chart_winchance_timeline.setDragEnabled(true);
                chart_winchance_timeline.setScaleEnabled(true);
                chart_winchance_timeline.setPinchZoom(true);
                chart_winchance_timeline.resetZoom();
                chart_winchance_timeline.getAxisRight().setEnabled(false);
                chart_winchance_timeline.setGridBackgroundColor(Color.TRANSPARENT);

                XAxis x = chart_winchance_timeline.getXAxis();
                x.setEnabled(false);

                YAxis y = chart_winchance_timeline.getAxisLeft();
                y.setLabelCount(11, false);
                y.setTextColor(Color.WHITE);
                y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                y.setDrawGridLines(true);
                y.setAxisLineColor(Color.WHITE);

                setWinChanceTimelineData(10, 100);

                TimeGraph1 timeGraph1 = jsonDetails.getTimeGraph1();

                Log.e("test", "onResponse: " + timeGraph1.getWinChance());
                Log.e("test1", "onResponse: " + matchid);


                String[] arrayWinChance= timeGraph1.getWinChance().split(",");
                ArrayList<Entry> valuesWinChance = new ArrayList<>();
                if (timeGraph1.getWinChance() != null && timeGraph1.getWinChance().length() > 0) {

                    for (int i = 1; i <= arrayWinChance.length; i++) {
                        valuesWinChance.add(new Entry(i, Float.parseFloat(arrayWinChance[i - 1])));
                    }

                    LineDataSet setWinChance;

                    if (chart_winchance_timeline.getData() != null &&
                            chart_winchance_timeline.getData().getDataSetCount() > 0) {
                        setWinChance = (LineDataSet) chart_winchance_timeline.getData().getDataSetByIndex(0);
                        chart_winchance_timeline.getData().notifyDataChanged();
                        chart_winchance_timeline.notifyDataSetChanged();
                    } else {
                        setWinChance = new LineDataSet(valuesWinChance, "Win Chance");
                        setWinChance.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setWinChance.setCubicIntensity(0.2f);
                        setWinChance.setDrawFilled(true);
                        setWinChance.setDrawCircles(false);
                        setWinChance.setLineWidth(1.8f);
                        setWinChance.setCircleRadius(4f);
                        setWinChance.setColor(getResources().getColor(R.color.red));
                        setWinChance.setFillAlpha(0);
                        setWinChance.setDrawHorizontalHighlightIndicator(false);

                        ld_winchance_timeline.addDataSet(setWinChance);
                        ld_winchance_timeline.setDrawValues(false);
                    }
                }

                chart_winchance_timeline.getLegend().setEnabled(true);
                chart_winchance_timeline.getLegend().setTextColor(Color.WHITE);
                chart_winchance_timeline.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//                chart_winchance_timeline.animateXY(2000, 2000);
                chart_winchance_timeline.getDescription().setEnabled(false);
                chart_winchance_timeline.setData(ld_winchance_timeline);
                chart_winchance_timeline.invalidate();
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
//kin
                //                Toast.makeText(WinChanceTimelineActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getTeamFightData(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                teamFightList = jsonDetails.getGraph4TeamFight();

                if (teamFightList != null && teamFightList.size() > 0) {

                    disables_radiant = Float.parseFloat(teamFightList.get(0).getDisablesRadiant());
                    disables_dire = Float.parseFloat(teamFightList.get(0).getDisablesDire());

                    nuke_radiant = Float.parseFloat(teamFightList.get(1).getNukeDmgRadiant());
                    nuke_dire = Float.parseFloat(teamFightList.get(1).getNukeDmgDire());

                    r_radiant = Float.parseFloat(teamFightList.get(2).getrDmgRadiant());
                    r_dire = Float.parseFloat(teamFightList.get(2).getrDmgDire());

                    durability_radiant = Float.parseFloat(teamFightList.get(3).getDurabilityRadiant());
                    durability_dire = Float.parseFloat(teamFightList.get(3).getDurabilityDire());

                    initiation_radiant = Float.parseFloat(teamFightList.get(4).getInitiationRadiant());
                    initiation_dire = Float.parseFloat(teamFightList.get(4).getInitiationDire());

                    healing_radiant = Float.parseFloat(teamFightList.get(5).getHealingRadiant());
                    healing_dire = Float.parseFloat(teamFightList.get(5).getHealingDire());

                    aura_radiant = Float.parseFloat(teamFightList.get(6).getAuraRadiant());
                    aura_dire = Float.parseFloat(teamFightList.get(6).getAuraDire());

                    createTeamFightChart(disables_radiant, disables_radiant + disables_dire,
                            nuke_radiant, nuke_radiant + nuke_dire,
                            r_radiant, r_radiant + r_dire,
                            durability_radiant, durability_radiant + durability_dire,
                            initiation_radiant, initiation_radiant + initiation_dire,
                            healing_radiant, healing_radiant + healing_dire,
                            aura_radiant, aura_radiant + aura_dire);
                } else {
                    createTeamFightChart(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(TeamFightActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createTeamFightChart(float disables_radiant, float disables_dire, float nuke_radiant, float nuke_dire, float r_radiant, float r_dire, float durability_radiant, float durability_dire, float initiation_radiant, float initiation_dire, float healing_radiant, float healing_dire, float aura_radiant, float aura_dire) {

        chart_teamfight.clear();

        chart_winchance.setVisibility(View.GONE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.VISIBLE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.GONE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.GONE);
        chart_draft_advantage.setVisibility(View.GONE);
        rl_statistics.setVisibility(View.GONE);
        ll_rd.setVisibility(View.GONE);

        getDataShow();

        //Add data for the first group
        direList.add(new BarEntry(1, disables_dire));
        direList.add(new BarEntry(2, nuke_dire));
        direList.add(new BarEntry(3, r_dire));
        direList.add(new BarEntry(4, durability_dire));
        direList.add(new BarEntry(5, initiation_dire));
        direList.add(new BarEntry(6, healing_dire));
        direList.add(new BarEntry(7, aura_dire));
        //Add data for the second group
        radiantList.add(new BarEntry(1, disables_radiant));
        radiantList.add(new BarEntry(2, nuke_radiant));
        radiantList.add(new BarEntry(3, r_radiant));
        radiantList.add(new BarEntry(4, durability_radiant));
        radiantList.add(new BarEntry(5, initiation_radiant));
        radiantList.add(new BarEntry(6, healing_radiant));
        radiantList.add(new BarEntry(7, aura_radiant));

        BarDataSet barDataSet2 = new BarDataSet(radiantList, "Radiant");
        barDataSet2.setColor(getResources().getColor(R.color.radiant_blue));
        BarDataSet barDataSet = new BarDataSet(direList, "Dire");
        barDataSet.setColor(getResources().getColor(R.color.dire_red));

        BarData barData = new BarData(barDataSet);
        barData.addDataSet(barDataSet2);
        barData.setValueTextColor(Color.TRANSPARENT);
        chart_teamfight.setData(barData);
        barData.setBarWidth(0.3f);

        chart_teamfight.getAxisLeft().setAxisMaximum(100);
        chart_teamfight.getAxisLeft().setAxisMinimum(0);
        chart_teamfight.getAxisLeft().setEnabled(false);
        chart_teamfight.getXAxis().setAxisMaximum(8);
        chart_teamfight.getXAxis().setAxisMinimum(0);
        chart_teamfight.getXAxis().setLabelCount(8, false);
        chart_teamfight.getXAxis().setValueFormatter(new IndexAxisValueFormatter(teamFightLabels));
        chart_teamfight.getDescription().setEnabled(false);
        chart_teamfight.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart_teamfight.getXAxis().setTextColor(Color.WHITE);
        chart_teamfight.getAxisRight().setAxisMaximum(100);
        chart_teamfight.getAxisRight().setAxisMinimum(0);
        chart_teamfight.getAxisRight().setEnabled(true);
        chart_teamfight.getAxisRight().setTextColor(Color.WHITE);
        chart_teamfight.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        chart_teamfight.getLegend().setTextColor(Color.WHITE);

    }

    private void setTeamFightTimelineData(int count, float range) {
        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(1, 99));
        values.add(new Entry(2, -99));

        LineDataSet set1;

        if (chart_teamfight_timeline.getData() != null &&
                chart_teamfight_timeline.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
            chart_teamfight_timeline.getData().notifyDataChanged();
            chart_teamfight_timeline.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(false);
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(4f);
            set1.setColor(Color.TRANSPARENT);
            set1.setFillAlpha(0);
            set1.setDrawHorizontalHighlightIndicator(false);
            ld_teamfight_timeline.addDataSet(set1);
            ld_teamfight_timeline.setDrawValues(false);
        }
    }

    public void getTeamFightTimeline(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                chart_teamfight_timeline.clear();

                chart_winchance.setVisibility(View.GONE);
                chart_winchance_timeline.setVisibility(View.GONE);
                chart_teamfight.setVisibility(View.GONE);
                chart_teamfight_timeline.setVisibility(View.VISIBLE);
                chart_liveadvantage.setVisibility(View.GONE);
                chart_liveadvantage_timeline.setVisibility(View.GONE);
                chart_total_advantage.setVisibility(View.GONE);
                chart_draft_advantage.setVisibility(View.GONE);
                rl_statistics.setVisibility(View.GONE);
                ll_rd.setVisibility(View.VISIBLE);
                getDataShow();

                chart_teamfight_timeline.setViewPortOffsets(100, 50, 0, 50);

                chart_teamfight_timeline.setTouchEnabled(true);
                chart_teamfight_timeline.setDragEnabled(true);
                chart_teamfight_timeline.setScaleEnabled(true);
                chart_teamfight_timeline.setPinchZoom(true);
                chart_teamfight_timeline.resetZoom();
                chart_teamfight_timeline.getAxisRight().setEnabled(false);
                chart_teamfight_timeline.setGridBackgroundColor(Color.TRANSPARENT);

                XAxis x = chart_teamfight_timeline.getXAxis();
                x.setEnabled(false);

                YAxis y = chart_teamfight_timeline.getAxisLeft();
                y.setLabelCount(11, false);
                y.setTextColor(Color.WHITE);
                y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                y.setDrawGridLines(true);
                y.setAxisLineColor(Color.WHITE);

                setTeamFightTimelineData(10, 100);
                TimeGraph3 timeGraph3 = jsonDetails.getTimeGraph3();

                //1
                String[] arrayDisables = timeGraph3.getDisables().split(",");
                ArrayList<Entry> valuesDisables = new ArrayList<>();
                if (timeGraph3.getDisables() != null && timeGraph3.getDisables().length() > 0) {
                    for (int i = 1; i <= arrayDisables.length; i++) {
                        valuesDisables.add(new Entry(i, Float.parseFloat(arrayDisables[i - 1])));
                    }
                    LineDataSet setDisables;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setDisables = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setDisables = new LineDataSet(valuesDisables, "Disables");
                        setDisables.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setDisables.setCubicIntensity(0.2f);
                        setDisables.setDrawFilled(true);
                        setDisables.setDrawCircles(false);
                        setDisables.setLineWidth(1.8f);
                        setDisables.setCircleRadius(4f);
                        setDisables.setColor(getResources().getColor(R.color.red));
                        setDisables.setFillAlpha(0);
                        setDisables.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setDisables);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //2
                String[] arrayNukeDmg = timeGraph3.getNukeDmg().split(",");
                ArrayList<Entry> valuesNukeDmg = new ArrayList<>();
                if (timeGraph3.getNukeDmg() != null && timeGraph3.getNukeDmg().length() > 0) {
                    for (int i = 1; i <= arrayNukeDmg.length; i++) {
                        valuesNukeDmg.add(new Entry(i, Float.parseFloat(arrayNukeDmg[i - 1])));
                    }
                    LineDataSet setNukeDmg;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setNukeDmg = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setNukeDmg = new LineDataSet(valuesNukeDmg, "Nuke Dmg");
                        setNukeDmg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setNukeDmg.setCubicIntensity(0.2f);
                        setNukeDmg.setDrawFilled(true);
                        setNukeDmg.setDrawCircles(false);
                        setNukeDmg.setLineWidth(1.8f);
                        setNukeDmg.setCircleRadius(4f);
                        setNukeDmg.setColor(getResources().getColor(R.color.purple));
                        setNukeDmg.setFillAlpha(0);
                        setNukeDmg.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setNukeDmg);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //3
                String[] arrayRDmg = timeGraph3.getRDmg().split(",");
                ArrayList<Entry> valuesRDmg = new ArrayList<>();
                if (timeGraph3.getRDmg() != null && timeGraph3.getRDmg().length() > 0) {
                    for (int i = 1; i <= arrayRDmg.length; i++) {
                        valuesRDmg.add(new Entry(i, Float.parseFloat(arrayRDmg[i - 1])));
                    }
                    LineDataSet setRDmg;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setRDmg = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setRDmg = new LineDataSet(valuesRDmg, "R Dmg");
                        setRDmg.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setRDmg.setCubicIntensity(0.2f);
                        setRDmg.setDrawFilled(true);
                        setRDmg.setDrawCircles(false);
                        setRDmg.setLineWidth(1.8f);
                        setRDmg.setCircleRadius(4f);
                        setRDmg.setColor(getResources().getColor(R.color.blue));
                        setRDmg.setFillAlpha(0);
                        setRDmg.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setRDmg);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //4
                String[] arrayDurability = timeGraph3.getDurability().split(",");
                ArrayList<Entry> valuesDurability = new ArrayList<>();
                if (timeGraph3.getDurability() != null && timeGraph3.getDurability().length() > 0) {
                    for (int i = 1; i <= arrayDurability.length; i++) {
                        valuesDurability.add(new Entry(i, Float.parseFloat(arrayDurability[i - 1])));
                    }
                    LineDataSet setDurability;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setDurability = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setDurability = new LineDataSet(valuesDurability, "Durability");
                        setDurability.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setDurability.setCubicIntensity(0.2f);
                        setDurability.setDrawFilled(true);
                        setDurability.setDrawCircles(false);
                        setDurability.setLineWidth(1.8f);
                        setDurability.setCircleRadius(4f);
                        setDurability.setColor(getResources().getColor(R.color.yellow));
                        setDurability.setFillAlpha(0);
                        setDurability.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setDurability);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //5
                String[] arrayInitiation = timeGraph3.getInitiation().split(",");
                ArrayList<Entry> valuesInitiation = new ArrayList<>();
                if (timeGraph3.getInitiation() != null && timeGraph3.getInitiation().length() > 0) {
                    for (int i = 1; i <= arrayInitiation.length; i++) {
                        valuesInitiation.add(new Entry(i, Float.parseFloat(arrayInitiation[i - 1])));
                    }
                    LineDataSet setInitiation;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setInitiation = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setInitiation = new LineDataSet(valuesInitiation, "Initiation");
                        setInitiation.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setInitiation.setCubicIntensity(0.2f);
                        setInitiation.setDrawFilled(true);
                        setInitiation.setDrawCircles(false);
                        setInitiation.setLineWidth(1.8f);
                        setInitiation.setCircleRadius(4f);
                        setInitiation.setColor(getResources().getColor(R.color.skyblue));
                        setInitiation.setFillAlpha(0);
                        setInitiation.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setInitiation);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }
                //6
                String[] arrayHealing = timeGraph3.getHealing().split(",");
                ArrayList<Entry> valuesHealing = new ArrayList<>();
                if (timeGraph3.getHealing() != null && timeGraph3.getHealing().length() > 0) {
                    for (int i = 1; i <= arrayHealing.length; i++) {
                        valuesHealing.add(new Entry(i, Float.parseFloat(arrayHealing[i - 1])));
                    }
                    LineDataSet setHealing;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setHealing = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setHealing = new LineDataSet(valuesHealing, "Healing");
                        setHealing.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setHealing.setCubicIntensity(0.2f);
                        setHealing.setDrawFilled(true);
                        setHealing.setDrawCircles(false);
                        setHealing.setLineWidth(1.8f);
                        setHealing.setCircleRadius(4f);
                        setHealing.setColor(getResources().getColor(R.color.green));
                        setHealing.setFillAlpha(0);
                        setHealing.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setHealing);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //7
                String[] arrayAura = timeGraph3.getAura().split(",");
                ArrayList<Entry> valuesAura = new ArrayList<>();
                if (timeGraph3.getAura() != null && timeGraph3.getAura().length() > 0) {
                    for (int i = 1; i <= arrayAura.length; i++) {
                        valuesAura.add(new Entry(i, Float.parseFloat(arrayAura[i - 1])));
                    }
                    LineDataSet setAura;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setAura = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setAura = new LineDataSet(valuesAura, "Aura");
                        setAura.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setAura.setCubicIntensity(0.2f);
                        setAura.setDrawFilled(true);
                        setAura.setDrawCircles(false);
                        setAura.setLineWidth(1.8f);
                        setAura.setCircleRadius(4f);
                        setAura.setColor(getResources().getColor(R.color.pink));
                        setAura.setFillAlpha(0);
                        setAura.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setAura);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //8
                String[] arrayTeamFightTotal = timeGraph3.getTeamFightTotal().split(",");
                ArrayList<Entry> valuesTeamFightTotal = new ArrayList<>();
                if (timeGraph3.getTeamFightTotal() != null && timeGraph3.getTeamFightTotal().length() > 0) {
                    for (int i = 1; i <= arrayTeamFightTotal.length; i++) {
                        valuesTeamFightTotal.add(new Entry(i, Float.parseFloat(arrayTeamFightTotal[i - 1])));
                    }
                    LineDataSet setTeamFightTotal;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setTeamFightTotal = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setTeamFightTotal = new LineDataSet(valuesTeamFightTotal, "Team Fight total");
                        setTeamFightTotal.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setTeamFightTotal.setCubicIntensity(0.2f);
                        setTeamFightTotal.setDrawFilled(true);
                        setTeamFightTotal.setDrawCircles(false);
                        setTeamFightTotal.setLineWidth(1.8f);
                        setTeamFightTotal.setCircleRadius(4f);
                        setTeamFightTotal.setColor(getResources().getColor(R.color.orangeyellow));
                        setTeamFightTotal.setFillAlpha(0);
                        setTeamFightTotal.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setTeamFightTotal);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //9
                String[] arrayMobility = timeGraph3.getMobility().split(",");
                ArrayList<Entry> valuesMobility = new ArrayList<>();
                if (timeGraph3.getMobility() != null && timeGraph3.getMobility().length() > 0) {
                    for (int i = 1; i <= arrayMobility.length; i++) {
                        valuesMobility.add(new Entry(i, Float.parseFloat(arrayMobility[i - 1])));
                    }
                    LineDataSet setMobility;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setMobility = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setMobility = new LineDataSet(valuesMobility, "Mobility");
                        setMobility.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setMobility.setCubicIntensity(0.2f);
                        setMobility.setDrawFilled(true);
                        setMobility.setDrawCircles(false);
                        setMobility.setLineWidth(1.8f);
                        setMobility.setCircleRadius(4f);
                        setMobility.setColor(getResources().getColor(R.color.teal));
                        setMobility.setFillAlpha(0);
                        setMobility.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setMobility);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                //10
                String[] arrayFastFarmer = timeGraph3.getFastFarmer().split(",");
                ArrayList<Entry> valuesFastFarmer = new ArrayList<>();
                if (timeGraph3.getFastFarmer() != null && timeGraph3.getFastFarmer().length() > 0) {
                    for (int i = 1; i <= arrayFastFarmer.length; i++) {
                        valuesFastFarmer.add(new Entry(i, Float.parseFloat(arrayFastFarmer[i - 1])));
                    }
                    LineDataSet setFastFarmer;

                    if (chart_teamfight_timeline.getData() != null &&
                            chart_teamfight_timeline.getData().getDataSetCount() > 0) {
                        setFastFarmer = (LineDataSet) chart_teamfight_timeline.getData().getDataSetByIndex(0);
                        chart_teamfight_timeline.getData().notifyDataChanged();
                        chart_teamfight_timeline.notifyDataSetChanged();
                    } else {
                        setFastFarmer = new LineDataSet(valuesFastFarmer, "Fast Farmer");
                        setFastFarmer.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setFastFarmer.setCubicIntensity(0.2f);
                        setFastFarmer.setDrawFilled(true);
                        setFastFarmer.setDrawCircles(false);
                        setFastFarmer.setLineWidth(1.8f);
                        setFastFarmer.setCircleRadius(4f);
                        setFastFarmer.setColor(getResources().getColor(R.color.darkgrey));
                        setFastFarmer.setFillAlpha(0);
                        setFastFarmer.setDrawHorizontalHighlightIndicator(false);

                        ld_teamfight_timeline.addDataSet(setFastFarmer);
                        ld_teamfight_timeline.setDrawValues(false);
                    }
                }

                chart_teamfight_timeline.getLegend().setEnabled(true);
                chart_teamfight_timeline.getLegend().setTextColor(Color.WHITE);
                chart_teamfight_timeline.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//                chart.animateXY(2000, 2000);
                chart_teamfight_timeline.getDescription().setEnabled(false);
                chart_teamfight_timeline.setData(ld_teamfight_timeline);
                chart_teamfight_timeline.invalidate();
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(TeamFightTimelineActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getLiveAdvantageData(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                liveAdvantageList = jsonDetails.getGraph2LiveAdvantage();

                if (liveAdvantageList != null && liveAdvantageList.size() > 0) {

                    teamFight_Radiant = Float.parseFloat(liveAdvantageList.get(0).getTeamFightRadiant());
                    teamFight_Dire = Float.parseFloat(liveAdvantageList.get(0).getTeamFightDire());

                    pushing_Radiant = Float.parseFloat(liveAdvantageList.get(1).getPushingRadiant());
                    pushing_Dire = Float.parseFloat(liveAdvantageList.get(1).getPushingDire());

                    defending_Radiant = Float.parseFloat(liveAdvantageList.get(2).getDefendingRadiant());
                    defending_Dire = Float.parseFloat(liveAdvantageList.get(2).getDefendingDire());

                    splitPushing_Radiant = Float.parseFloat(liveAdvantageList.get(3).getSplitPushingRadiant());
                    splitPushing_Dire = Float.parseFloat(liveAdvantageList.get(3).getSplitPushingDire());

                    createLiveAdvantageChart(teamFight_Radiant, teamFight_Radiant + teamFight_Dire,
                            pushing_Radiant, pushing_Radiant + pushing_Dire,
                            defending_Radiant, defending_Radiant + defending_Dire,
                            splitPushing_Radiant, splitPushing_Radiant + splitPushing_Dire);

                } else {
                    createLiveAdvantageChart(0, 0, 0, 0, 0, 0, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(LiveAdvantageActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createLiveAdvantageChart(float teamFight_Radiant, float teamFight_dire, float pushing_Radiant, float pushing_dire, float defending_Radiant, float defending_dire, float splitPushing_Radiant, float splitPushing_dire) {

        chart_liveadvantage.clear();

        chart_winchance.setVisibility(View.GONE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.GONE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.VISIBLE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.GONE);
        chart_draft_advantage.setVisibility(View.GONE);
        rl_statistics.setVisibility(View.GONE);
        ll_rd.setVisibility(View.GONE);

        getDataShow();

        direList.add(new BarEntry(1, splitPushing_dire));
        direList.add(new BarEntry(2, defending_dire));
        direList.add(new BarEntry(3, pushing_dire));
        direList.add(new BarEntry(4, teamFight_dire));

        radiantList.add(new BarEntry(1, splitPushing_Radiant));
        radiantList.add(new BarEntry(2, defending_Radiant));
        radiantList.add(new BarEntry(3, pushing_Radiant));
        radiantList.add(new BarEntry(4, teamFight_Radiant));

        BarDataSet barDataSet2 = new BarDataSet(radiantList, "Radiant");
        barDataSet2.setColor(getResources().getColor(R.color.radiant_blue));
        BarDataSet barDataSet = new BarDataSet(direList, "Dire");
        barDataSet.setColor(getResources().getColor(R.color.dire_red));
        BarData barData = new BarData(barDataSet);

        barData.addDataSet(barDataSet2);
        barData.setValueTextColor(Color.TRANSPARENT);
        chart_liveadvantage.setData(barData);
        barData.setBarWidth(0.4f);

        chart_liveadvantage.getAxisLeft().setAxisMaximum(100);
        chart_liveadvantage.getAxisLeft().setAxisMinimum(0);
        chart_liveadvantage.getAxisLeft().setEnabled(false);
        chart_liveadvantage.getXAxis().setAxisMaximum(5);
        chart_liveadvantage.getXAxis().setAxisMinimum(0);
        chart_liveadvantage.getXAxis().setLabelCount(5, false);
        chart_liveadvantage.getXAxis().setValueFormatter(new IndexAxisValueFormatter(liveAdvantageLabels));
        chart_liveadvantage.getDescription().setEnabled(false);
        chart_liveadvantage.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart_liveadvantage.getXAxis().setTextColor(Color.WHITE);
        chart_liveadvantage.getAxisRight().setAxisMaximum(100);
        chart_liveadvantage.getAxisRight().setAxisMinimum(0);
        chart_liveadvantage.getAxisRight().setEnabled(true);
        chart_liveadvantage.getAxisRight().setTextColor(Color.WHITE);
        chart_liveadvantage.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        chart_liveadvantage.getLegend().setTextColor(Color.WHITE);

    }

    private void setLiveAdvantageTimelineData(int count, float range) {
        ArrayList<Entry> values = new ArrayList<>();
        values.add(new Entry(1, 99));
        values.add(new Entry(2, -99));

        LineDataSet set1;

        if (chart_liveadvantage_timeline.getData() != null &&
                chart_liveadvantage_timeline.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart_liveadvantage_timeline.getData().getDataSetByIndex(0);
            chart_liveadvantage_timeline.getData().notifyDataChanged();
            chart_liveadvantage_timeline.notifyDataSetChanged();
        } else {
            set1 = new LineDataSet(values, "");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(false);
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(4f);
            set1.setColor(Color.TRANSPARENT);
            set1.setFillAlpha(0);
            set1.setDrawHorizontalHighlightIndicator(false);
            ld_liveadvantage_timeline.addDataSet(set1);
            ld_liveadvantage_timeline.setDrawValues(false);
        }
    }

    public void getLiveAdvantageTimeline(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                chart_liveadvantage_timeline.clear();

                chart_winchance.setVisibility(View.GONE);
                chart_winchance_timeline.setVisibility(View.GONE);
                chart_teamfight.setVisibility(View.GONE);
                chart_teamfight_timeline.setVisibility(View.GONE);
                chart_liveadvantage.setVisibility(View.GONE);
                chart_liveadvantage_timeline.setVisibility(View.VISIBLE);
                chart_total_advantage.setVisibility(View.GONE);
                chart_draft_advantage.setVisibility(View.GONE);
                rl_statistics.setVisibility(View.GONE);
                ll_rd.setVisibility(View.VISIBLE);
                getDataShow();

                chart_liveadvantage_timeline.setViewPortOffsets(100, 50, 0, 50);

                chart_liveadvantage_timeline.setTouchEnabled(true);
                chart_liveadvantage_timeline.setDragEnabled(true);
                chart_liveadvantage_timeline.setScaleEnabled(true);
                chart_liveadvantage_timeline.setPinchZoom(true);
                chart_liveadvantage_timeline.resetZoom();
                chart_liveadvantage_timeline.getAxisRight().setEnabled(false);
                chart_liveadvantage_timeline.setGridBackgroundColor(Color.TRANSPARENT);

                XAxis x = chart_liveadvantage_timeline.getXAxis();
                x.setEnabled(false);

                YAxis y = chart_liveadvantage_timeline.getAxisLeft();
                y.setLabelCount(11, false);
                y.setTextColor(Color.WHITE);
                y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                y.setDrawGridLines(true);
                y.setAxisLineColor(Color.WHITE);


                setLiveAdvantageTimelineData(10, 100);

                TimeGraph2 timeGraph2 = jsonDetails.getTimeGraph2();
                //1
                String[] arrayTeamFight = timeGraph2.getTeamFight().split(",");
                ArrayList<Entry> valuesTeamFight = new ArrayList<>();
                if (timeGraph2.getTeamFight() != null && timeGraph2.getTeamFight().length() > 0) {
                    for (int i = 1; i <= arrayTeamFight.length; i++) {
                        valuesTeamFight.add(new Entry(i, Float.parseFloat(arrayTeamFight[i - 1])));
                    }
                    LineDataSet setTeamFight;

                    if (chart_liveadvantage_timeline.getData() != null && chart_liveadvantage_timeline.getData().getDataSetCount() > 0) {
                        setTeamFight = (LineDataSet) chart_liveadvantage_timeline.getData().getDataSetByIndex(0);
                        chart_liveadvantage_timeline.getData().notifyDataChanged();
                        chart_liveadvantage_timeline.notifyDataSetChanged();
                    } else {
                        setTeamFight = new LineDataSet(valuesTeamFight, "Team Fight");
                        setTeamFight.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setTeamFight.setCubicIntensity(0.2f);
                        setTeamFight.setDrawFilled(true);
                        setTeamFight.setDrawCircles(false);
                        setTeamFight.setLineWidth(1.8f);
                        setTeamFight.setCircleRadius(4f);
                        setTeamFight.setColor(getResources().getColor(R.color.red));
                        setTeamFight.setFillAlpha(0);
                        setTeamFight.setDrawHorizontalHighlightIndicator(false);

                        ld_liveadvantage_timeline.addDataSet(setTeamFight);
                        ld_liveadvantage_timeline.setDrawValues(false);
                    }
                }

                //2
                String[] arrayPushing = timeGraph2.getPushing().split(",");
                ArrayList<Entry> valuesPushing = new ArrayList<>();
                if (timeGraph2.getPushing() != null && timeGraph2.getPushing().length() > 0) {
                    for (int i = 1; i <= arrayPushing.length; i++) {
                        valuesPushing.add(new Entry(i, Float.parseFloat(arrayPushing[i - 1])));
                    }
                    LineDataSet setPushing;

                    if (chart_liveadvantage_timeline.getData() != null &&
                            chart_liveadvantage_timeline.getData().getDataSetCount() > 0) {
                        setPushing = (LineDataSet) chart_liveadvantage_timeline.getData().getDataSetByIndex(0);
                        chart_liveadvantage_timeline.getData().notifyDataChanged();
                        chart_liveadvantage_timeline.notifyDataSetChanged();
                    } else {
                        setPushing = new LineDataSet(valuesPushing, "Pushing");
                        setPushing.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setPushing.setCubicIntensity(0.2f);
                        setPushing.setDrawFilled(true);
                        setPushing.setDrawCircles(false);
                        setPushing.setLineWidth(1.8f);
                        setPushing.setCircleRadius(4f);
                        setPushing.setColor(getResources().getColor(R.color.purple));
                        setPushing.setFillAlpha(0);
                        setPushing.setDrawHorizontalHighlightIndicator(false);

                        ld_liveadvantage_timeline.addDataSet(setPushing);
                        ld_liveadvantage_timeline.setDrawValues(false);
                    }
                }

                //3
                String[] arrayDefending = timeGraph2.getDefending().split(",");
                ArrayList<Entry> valuesDefending = new ArrayList<>();
                if (timeGraph2.getDefending() != null && timeGraph2.getDefending().length() > 0) {
                    for (int i = 1; i <= arrayDefending.length; i++) {
                        valuesDefending.add(new Entry(i, Float.parseFloat(arrayDefending[i - 1])));
                    }
                    LineDataSet setDefending;

                    if (chart_liveadvantage_timeline.getData() != null &&
                            chart_liveadvantage_timeline.getData().getDataSetCount() > 0) {
                        setDefending = (LineDataSet) chart_liveadvantage_timeline.getData().getDataSetByIndex(0);
                        chart_liveadvantage_timeline.getData().notifyDataChanged();
                        chart_liveadvantage_timeline.notifyDataSetChanged();
                    } else {
                        setDefending = new LineDataSet(valuesDefending, "Defending");
                        setDefending.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setDefending.setCubicIntensity(0.2f);
                        setDefending.setDrawFilled(true);
                        setDefending.setDrawCircles(false);
                        setDefending.setLineWidth(1.8f);
                        setDefending.setCircleRadius(4f);
                        setDefending.setColor(getResources().getColor(R.color.blue));
                        setDefending.setFillAlpha(0);
                        setDefending.setDrawHorizontalHighlightIndicator(false);

                        ld_liveadvantage_timeline.addDataSet(setDefending);
                        ld_liveadvantage_timeline.setDrawValues(false);
                    }
                }

                //4
                String[] arraySplitPushing = timeGraph2.getSplitPushing().split(",");
                ArrayList<Entry> valuesSplitPushing = new ArrayList<>();
                if (timeGraph2.getSplitPushing() != null && timeGraph2.getSplitPushing().length() > 0) {
                    for (int i = 1; i <= arraySplitPushing.length; i++) {
                        valuesSplitPushing.add(new Entry(i, Float.parseFloat(arraySplitPushing[i - 1])));
                    }
                    LineDataSet setSplitPushing;

                    if (chart_liveadvantage_timeline.getData() != null &&
                            chart_liveadvantage_timeline.getData().getDataSetCount() > 0) {
                        setSplitPushing = (LineDataSet) chart_liveadvantage_timeline.getData().getDataSetByIndex(0);
                        chart_liveadvantage_timeline.getData().notifyDataChanged();
                        chart_liveadvantage_timeline.notifyDataSetChanged();
                    } else {
                        setSplitPushing = new LineDataSet(valuesSplitPushing, "Split Pushing");
                        setSplitPushing.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setSplitPushing.setCubicIntensity(0.2f);
                        setSplitPushing.setDrawFilled(true);
                        setSplitPushing.setDrawCircles(false);
                        setSplitPushing.setLineWidth(1.8f);
                        setSplitPushing.setCircleRadius(4f);
                        setSplitPushing.setColor(getResources().getColor(R.color.yellow));
                        setSplitPushing.setFillAlpha(0);
                        setSplitPushing.setDrawHorizontalHighlightIndicator(false);

                        ld_liveadvantage_timeline.addDataSet(setSplitPushing);
                        ld_liveadvantage_timeline.setDrawValues(false);
                    }
                }

                //5
                String[] arrayLiveAdvantage = timeGraph2.getLiveAdvantage().split(",");
                ArrayList<Entry> valuesLiveAdvantage = new ArrayList<>();
                if (timeGraph2.getLiveAdvantage() != null && timeGraph2.getLiveAdvantage().length() > 0) {
                    for (int i = 1; i <= arrayLiveAdvantage.length; i++) {
                        valuesLiveAdvantage.add(new Entry(i, Float.parseFloat(arrayLiveAdvantage[i - 1])));
                    }
                    LineDataSet setLiveAdvantage;

                    if (chart_liveadvantage_timeline.getData() != null &&
                            chart_liveadvantage_timeline.getData().getDataSetCount() > 0) {
                        setLiveAdvantage = (LineDataSet) chart_liveadvantage_timeline.getData().getDataSetByIndex(0);
                        chart_liveadvantage_timeline.getData().notifyDataChanged();
                        chart_liveadvantage_timeline.notifyDataSetChanged();
                    } else {
                        setLiveAdvantage = new LineDataSet(valuesLiveAdvantage, "Live Advantage");
                        setLiveAdvantage.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        setLiveAdvantage.setCubicIntensity(0.2f);
                        setLiveAdvantage.setDrawFilled(true);
                        setLiveAdvantage.setDrawCircles(false);
                        setLiveAdvantage.setLineWidth(1.8f);
                        setLiveAdvantage.setCircleRadius(4f);
                        setLiveAdvantage.setColor(getResources().getColor(R.color.skyblue));
                        setLiveAdvantage.setFillAlpha(0);
                        setLiveAdvantage.setDrawHorizontalHighlightIndicator(false);

                        ld_liveadvantage_timeline.addDataSet(setLiveAdvantage);
                        ld_liveadvantage_timeline.setDrawValues(false);
                    }
                }

                chart_liveadvantage_timeline.getLegend().setEnabled(true);
                chart_liveadvantage_timeline.getLegend().setTextColor(Color.WHITE);
                chart_liveadvantage_timeline.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
//                chart.animateXY(2000, 2000);
                chart_liveadvantage_timeline.getDescription().setEnabled(false);
                chart_liveadvantage_timeline.setData(ld_liveadvantage_timeline);
                chart_liveadvantage_timeline.invalidate();
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(LiveAdvantageTimelineActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getTotalAdvantageData(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                totalAdvantageList = jsonDetails.getGraph3TotalAdvantage();

                if (totalAdvantageList != null && totalAdvantageList.size() > 0) {

                    draft_radiant = Float.parseFloat(totalAdvantageList.get(0).getDraftAdvantageRadiant());
                    draft_dire = Float.parseFloat(totalAdvantageList.get(0).getDraftAdvantageDire());

                    live_radiant = Float.parseFloat(totalAdvantageList.get(1).getLiveAdvantageRadiant());
                    live_dire = Float.parseFloat(totalAdvantageList.get(1).getLiveAdvantageDire());

                    createTotalAdvantageChart(draft_radiant, draft_radiant + draft_dire, live_radiant, live_radiant + live_dire);
                } else {
                    createTotalAdvantageChart(0, 0, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(TotalAdvantageActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createTotalAdvantageChart(float draft_radiant, float draft_dire, float live_radiant, float live_dire) {//r(radient)->blue,d(dire)->red

        chart_total_advantage.clear();

        chart_winchance.setVisibility(View.GONE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.GONE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.GONE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.VISIBLE);
        chart_draft_advantage.setVisibility(View.GONE);
        rl_statistics.setVisibility(View.GONE);
        ll_rd.setVisibility(View.GONE);
        getDataShow();

        direList.add(new BarEntry(1, live_dire));
        direList.add(new BarEntry(2, draft_dire));

        radiantList.add(new BarEntry(1, live_radiant));
        radiantList.add(new BarEntry(2, draft_radiant));

        BarDataSet barDataSet2 = new BarDataSet(radiantList, "Radiant");
        barDataSet2.setColor(getResources().getColor(R.color.radiant_blue));
        BarDataSet barDataSet = new BarDataSet(direList, "Dire");
        barDataSet.setColor(getResources().getColor(R.color.dire_red));
        BarData barData = new BarData(barDataSet);

        barData.addDataSet(barDataSet2);
        barData.setValueTextColor(Color.TRANSPARENT);
        chart_total_advantage.setData(barData);
        barData.setBarWidth(0.4f);

        chart_total_advantage.getAxisLeft().setAxisMaximum(100);
        chart_total_advantage.getAxisLeft().setAxisMinimum(0);
        chart_total_advantage.getAxisLeft().setEnabled(false);
        chart_total_advantage.getXAxis().setAxisMaximum(3);
        chart_total_advantage.getXAxis().setAxisMinimum(0);
        chart_total_advantage.getXAxis().setLabelCount(3, false);
        chart_total_advantage.getXAxis().setValueFormatter(new IndexAxisValueFormatter(labels));
        chart_total_advantage.getDescription().setEnabled(false);
        chart_total_advantage.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart_total_advantage.getXAxis().setTextColor(Color.WHITE);
        chart_total_advantage.getAxisRight().setAxisMaximum(100);
        chart_total_advantage.getAxisRight().setAxisMinimum(0);
        chart_total_advantage.getAxisRight().setEnabled(true);
        chart_total_advantage.getAxisRight().setTextColor(Color.WHITE);
        chart_total_advantage.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        chart_total_advantage.getLegend().setTextColor(Color.WHITE);
    }

    public void getDraftAdvantageData(String matchid) {
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                draftAdvantageList = jsonDetails.getGraph1DraftAdvantage();

                if (draftAdvantageList != null && draftAdvantageList.size() > 0) {

                    disables_radiant = Float.parseFloat(draftAdvantageList.get(0).getDisablesRadiant());
                    disables_dire = Float.parseFloat(draftAdvantageList.get(0).getDisablesDire());

                    nukes_radiant = Float.parseFloat(draftAdvantageList.get(1).getNukesRadiant());
                    nukes_dire = Float.parseFloat(draftAdvantageList.get(1).getNukesDire());

                    pushing_radiant = Float.parseFloat(draftAdvantageList.get(2).getPushingRadiant());
                    pushing_dire = Float.parseFloat(draftAdvantageList.get(2).getPushingDire());

                    defending_radiant = Float.parseFloat(draftAdvantageList.get(3).getDefendingRadiant());
                    defending_dire = Float.parseFloat(draftAdvantageList.get(3).getDefendingDire());

                    splitPushing_radiant = Float.parseFloat(draftAdvantageList.get(4).getSplitPushingRadiant());
                    splitPushing_dire = Float.parseFloat(draftAdvantageList.get(4).getSplitPushingDire());

                    rDmg_radiant = Float.parseFloat(draftAdvantageList.get(5).getrDmgRadiant());
                    rDmg_dire = Float.parseFloat(draftAdvantageList.get(5).getrDmgDire());

                    durability_radiant = Float.parseFloat(draftAdvantageList.get(6).getDurabilityRadiant());
                    durability_dire = Float.parseFloat(draftAdvantageList.get(6).getDurabilityDire());

                    mobility_radiant = Float.parseFloat(draftAdvantageList.get(7).getMobilityRadiant());
                    mobility_dire = Float.parseFloat(draftAdvantageList.get(7).getMobilityDire());

                    healing_radiant = Float.parseFloat(draftAdvantageList.get(8).getHealingRadiant());
                    healing_dire = Float.parseFloat(draftAdvantageList.get(8).getHealingDire());

                    initiation_radiant = Float.parseFloat(draftAdvantageList.get(9).getInitiationRadiant());
                    initiation_dire = Float.parseFloat(draftAdvantageList.get(9).getInitiationDire());

                    counters_radiant = Float.parseFloat(draftAdvantageList.get(10).getCountersRadiant());
                    counters_dire = Float.parseFloat(draftAdvantageList.get(10).getCountersDire());

                    createDraftAdvantageChart(disables_radiant, disables_radiant + disables_dire,
                            nukes_radiant, nukes_radiant + nukes_dire,
                            pushing_radiant, pushing_radiant + pushing_dire,
                            defending_radiant, defending_radiant + defending_dire,
                            splitPushing_radiant, splitPushing_radiant + splitPushing_dire,
                            rDmg_radiant, rDmg_radiant + rDmg_dire,
                            durability_radiant, durability_radiant + durability_dire,
                            mobility_radiant, mobility_radiant + mobility_dire,
                            healing_radiant, healing_radiant + healing_dire,
                            initiation_radiant, initiation_radiant + initiation_dire,
                            counters_radiant, counters_radiant + counters_dire);
                } else {
                    createDraftAdvantageChart(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
                //kin
//                Toast.makeText(DraftAdvantageActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void createDraftAdvantageChart(float disables_radiant, float disables_dire, float nukes_radiant, float nukes_dire, float pushing_radiant, float pushing_dire, float defending_radiant, float defending_dire, float splitPushing_radiant, float splitPushing_dire, float rDmg_radiant, float rDmg_dire, float durability_radiant, float durability_dire, float mobility_radiant, float mobility_dire, float healing_radiant, float healing_dire, float initiation_radiant, float initiation_dire, float counters_radiant, float counters_dire) {
        chart_draft_advantage.clear();

        chart_winchance.setVisibility(View.GONE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.GONE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.GONE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.GONE);
        chart_draft_advantage.setVisibility(View.VISIBLE);
        rl_statistics.setVisibility(View.GONE);
        ll_rd.setVisibility(View.GONE);
        getDataShow();

        direList.add(new BarEntry(1, counters_dire));
        direList.add(new BarEntry(2, initiation_dire));
        direList.add(new BarEntry(3, healing_dire));
        direList.add(new BarEntry(4, mobility_dire));
        direList.add(new BarEntry(5, durability_dire));
        direList.add(new BarEntry(6, rDmg_dire));
        direList.add(new BarEntry(7, splitPushing_dire));
        direList.add(new BarEntry(8, defending_dire));
        direList.add(new BarEntry(9, pushing_dire));
        direList.add(new BarEntry(10, nukes_dire));
        direList.add(new BarEntry(11, disables_dire));

        radiantList.add(new BarEntry(1, counters_radiant));
        radiantList.add(new BarEntry(2, initiation_radiant));
        radiantList.add(new BarEntry(3, healing_radiant));
        radiantList.add(new BarEntry(4, mobility_radiant));
        radiantList.add(new BarEntry(5, durability_radiant));
        radiantList.add(new BarEntry(6, rDmg_radiant));
        radiantList.add(new BarEntry(7, splitPushing_radiant));
        radiantList.add(new BarEntry(8, defending_radiant));
        radiantList.add(new BarEntry(9, pushing_radiant));
        radiantList.add(new BarEntry(10, nukes_radiant));
        radiantList.add(new BarEntry(11, disables_radiant));

        BarDataSet barDataSet2 = new BarDataSet(radiantList, "Radiant");
        barDataSet2.setColor(getResources().getColor(R.color.radiant_blue));
        BarDataSet barDataSet = new BarDataSet(direList, "Dire");
        barDataSet.setColor(getResources().getColor(R.color.dire_red));
        BarData barData = new BarData(barDataSet);

        barData.addDataSet(barDataSet2);
        barData.setValueTextColor(Color.TRANSPARENT);
        chart_draft_advantage.setData(barData);
        barData.setBarWidth(0.2f);

        chart_draft_advantage.getAxisLeft().setAxisMaximum(100);
        chart_draft_advantage.getAxisLeft().setAxisMinimum(0);
        chart_draft_advantage.getAxisLeft().setEnabled(false);
        chart_draft_advantage.getXAxis().setAxisMaximum(12);
        chart_draft_advantage.getXAxis().setAxisMinimum(0);
        chart_draft_advantage.getXAxis().setLabelCount(12, false);
        chart_draft_advantage.getXAxis().setValueFormatter(new IndexAxisValueFormatter(DraftAdvantageLabels));
        chart_draft_advantage.getDescription().setEnabled(false);
        chart_draft_advantage.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart_draft_advantage.getXAxis().setTextColor(Color.WHITE);
        chart_draft_advantage.getAxisRight().setAxisMaximum(100);
        chart_draft_advantage.getAxisRight().setAxisMinimum(0);
        chart_draft_advantage.getAxisRight().setEnabled(true);
        chart_draft_advantage.getAxisRight().setTextColor(Color.WHITE);
        chart_draft_advantage.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        chart_draft_advantage.getLegend().setTextColor(Color.WHITE);

    }

    public void getStatisticsData(String matchid) {

        chart_winchance.setVisibility(View.GONE);
        chart_winchance_timeline.setVisibility(View.GONE);
        chart_teamfight.setVisibility(View.GONE);
        chart_teamfight_timeline.setVisibility(View.GONE);
        chart_liveadvantage.setVisibility(View.GONE);
        chart_liveadvantage_timeline.setVisibility(View.GONE);
        chart_total_advantage.setVisibility(View.GONE);
        chart_draft_advantage.setVisibility(View.GONE);
        rl_statistics.setVisibility(View.VISIBLE);
        ll_rd.setVisibility(View.GONE);
        getDataShow();

//        lottie_progress.setVisibility(View.VISIBLE);
//        tv_no_data.setVisibility(View.GONE);
//        rv_statistics.setVisibility(View.GONE);
        API.graphlist().getquestionsrelateddetails(matchid).enqueue(new retrofit2.Callback<CommonTime.JsonDetails>() {
            @Override
            public void onResponse(@NonNull Call<CommonTime.JsonDetails> call, @NonNull Response<CommonTime.JsonDetails> response) {
                CommonTime.JsonDetails jsonDetails = response.body();
                assert jsonDetails != null;

                table1ArrayList = jsonDetails.getTable1();
                heroIDAndNameList = jsonDetails.getHeroIDAndNames();

//                lottie_progress.setVisibility(View.GONE);
//                tv_no_data.setVisibility(View.GONE);
//                rv_statistics.setVisibility(View.VISIBLE);

                if (table1ArrayList != null && table1ArrayList.size() > 0) {
                    adapter = new StatisticsAdapter(LiveAnalysisDetailsActivity.this, table1ArrayList);
                    layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                    rv_statistics.setLayoutManager(layoutManager);
                    rv_statistics.setItemAnimator(new DefaultItemAnimator());
                    rv_statistics.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                } else {
//                    lottie_progress.setVisibility(View.GONE);
//                    tv_no_data.setVisibility(View.VISIBLE);
//                    rv_statistics.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CommonTime.JsonDetails> call, Throwable t) {
//                Toast.makeText(LiveAnalysisActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//                lottie_progress.setVisibility(View.GONE);
//                tv_no_data.setVisibility(View.VISIBLE);
//                rv_statistics.setVisibility(View.GONE);
            }
        });
    }




}