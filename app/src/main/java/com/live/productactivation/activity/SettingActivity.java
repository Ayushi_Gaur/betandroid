package com.live.productactivation.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.PrefManager;

public class SettingActivity extends BaseActivity {

    private LinearLayout ll_signout;
    PrefManager prefManager;
    private ImageView imgbg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }


        prefManager = new PrefManager(this);
        ll_signout = findViewById(R.id.ll_signout);

        ll_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignInClient.signOut();
                prefManager.setUserPhoto(null);
                prefManager.setUserName(null);
                prefManager.setUserToken(null);
                startActivity(new Intent(SettingActivity.this, LoginActivity.class));
             //kin
                //   Toast.makeText(SettingActivity.this, "Sign Out", Toast.LENGTH_SHORT).show();
                ll_signout.setVisibility(View.INVISIBLE);
            }
        });

    }
}