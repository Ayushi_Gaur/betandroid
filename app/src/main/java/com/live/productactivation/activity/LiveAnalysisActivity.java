package com.live.productactivation.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.live.productactivation.R;
import com.live.productactivation.adapter.LiveAnalysisAdapter;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.model.MatchListDetail;
import com.live.productactivation.rest.API;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class LiveAnalysisActivity extends BaseActivity {

    private RecyclerView rv_live_analysis;
    private LiveAnalysisAdapter adapter;
    private LinearLayoutManager layoutManager;
    private LottieAnimationView lottie_progress;
    private TextView tv_no_data;
    private ImageView imgbg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liveanalysis);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        rv_live_analysis = findViewById(R.id.rv_live_analysis);
        lottie_progress = findViewById(R.id.lottie_progress);
        tv_no_data = findViewById(R.id.tv_no_data);
        getResultData();
    }

    public void getResultData() {
        lottie_progress.setVisibility(View.VISIBLE);
        tv_no_data.setVisibility(View.GONE);
        rv_live_analysis.setVisibility(View.GONE);
        API.matchList().getmatchlistdetails().enqueue(new retrofit2.Callback<List<MatchListDetail>>() {
            @Override
            public void onResponse(@NonNull Call<List<MatchListDetail>> call, @NonNull Response<List<MatchListDetail>> response) {
                List<MatchListDetail> matchListDetail = response.body();

                assert matchListDetail != null;
                lottie_progress.setVisibility(View.GONE);
                tv_no_data.setVisibility(View.GONE);
                rv_live_analysis.setVisibility(View.VISIBLE);
                if (matchListDetail != null && matchListDetail.size() > 0) {
                    adapter = new LiveAnalysisAdapter(LiveAnalysisActivity.this, matchListDetail);
                    layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                    rv_live_analysis.setLayoutManager(layoutManager);
                    rv_live_analysis.setItemAnimator(new DefaultItemAnimator());
                    rv_live_analysis.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    lottie_progress.setVisibility(View.GONE);
                    tv_no_data.setVisibility(View.VISIBLE);
                    rv_live_analysis.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<MatchListDetail>> call, Throwable t) {
                lottie_progress.setVisibility(View.GONE);
                tv_no_data.setVisibility(View.VISIBLE);
                rv_live_analysis.setVisibility(View.GONE);
            }
        });
    }
}