package com.live.productactivation.activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.live.productactivation.R;
import com.live.productactivation.adapter.LeaderBoardAdapter;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.PrefManager;
import com.live.productactivation.model.Data;
import com.live.productactivation.model.GetTopUsers;
import com.live.productactivation.model.Users;
import com.live.productactivation.rest.LoginAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class LeaderBoardActivity extends BaseActivity {

    private RecyclerView rv_leaderboard;
    private LeaderBoardAdapter adapter;
    private ImageView imgbg;

    private LinearLayoutManager layoutManager;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                |View.SYSTEM_UI_FLAG_FULLSCREEN
                |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        prefManager = new PrefManager(this);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        rv_leaderboard = findViewById(R.id.rv_leaderboard);
        Log.e("token", "onCreate: " + prefManager.getUserToken());
        getLeaderboarddata(prefManager.getUserToken());

    }

    public void getLeaderboarddata(String token) {
        LoginAPI.login().getGetTopUsers(token.trim()).enqueue(new retrofit2.Callback<GetTopUsers>() {
            @Override
            public void onResponse(@NonNull Call<GetTopUsers> call, @NonNull Response<GetTopUsers> response) {
                GetTopUsers getTopUsers = response.body();
                assert getTopUsers != null;
                List<Users> usersList = getTopUsers.getData().getUsersList();

                if (usersList != null && usersList.size() > 0) {
                 //kin
                    //   Toast.makeText(LeaderBoardActivity.this, getTopUsers.getMessage(), Toast.LENGTH_SHORT).show();

                    usersList = getTopUsers.getData().getUsersList();
                    adapter = new LeaderBoardAdapter(LeaderBoardActivity.this, usersList);
                    layoutManager = new LinearLayoutManager(LeaderBoardActivity.this);
                    layoutManager.setOrientation(RecyclerView.VERTICAL);
                    rv_leaderboard.setLayoutManager(layoutManager);
                    rv_leaderboard.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<GetTopUsers> call, Throwable t) {
             //kin
//                Toast.makeText(LeaderBoardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}