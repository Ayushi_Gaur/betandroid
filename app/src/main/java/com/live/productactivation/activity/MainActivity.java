package com.live.productactivation.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.live.productactivation.BuildConfig;
import com.live.productactivation.R;
import com.live.productactivation.common.BaseActivity;
import com.live.productactivation.common.PrefManager;
import com.live.productactivation.model.GetTopUsers;
import com.live.productactivation.rest.LoginAPI;
import com.mikhaellopez.circularimageview.CircularImageView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private CircularImageView iv_profile;
    private ImageView iv_settings, iv_about,imgbg;
    private LinearLayout ll_liveAnalysis, ll_freeBetting, ll_leaderboard, ll_enterInvitation,ll_enternews;
    private int perDayCoin;
    private PrefManager prefManager;
    String givenName, photo;
    JsonObject paramObject;
    ArrayList<String> rssLinks = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rssLinks.add("https://estnn.com/feed/");

        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        |View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                |View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        |View.SYSTEM_UI_FLAG_FULLSCREEN
        |View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);



                prefManager = new PrefManager(this);

        imgbg = findViewById(R.id.imgbg);
        int orientation = getResources().getConfiguration().orientation;
        if(orientation == Configuration.ORIENTATION_LANDSCAPE) {
            imgbg.setImageResource(R.drawable.portbg);
        } else {
            imgbg.setImageResource(R.drawable.appbg);
        }

        iv_profile = findViewById(R.id.iv_profile);
        iv_settings = findViewById(R.id.iv_settings);
        iv_about = findViewById(R.id.iv_about);
        ll_liveAnalysis = findViewById(R.id.ll_liveAnalysis);
        ll_freeBetting = findViewById(R.id.ll_freeBetting);
        ll_leaderboard = findViewById(R.id.ll_leaderboard);
        ll_enterInvitation = findViewById(R.id.ll_enterInvitation);
        ll_enternews = findViewById(R.id.ll_enternews);

        Glide.with(this).load(prefManager.getUserPhoto()).placeholder(R.drawable.ic_profile).into(iv_profile);

        iv_profile.setOnClickListener(this);
        iv_settings.setOnClickListener(this);
        iv_about.setOnClickListener(this);
        ll_liveAnalysis.setOnClickListener(this);
        ll_freeBetting.setOnClickListener(this);
        ll_leaderboard.setOnClickListener(this);
        ll_enterInvitation.setOnClickListener(this);
        ll_enternews.setOnClickListener(this);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        String todayString = year + "" + month + "" + day;
        SharedPreferences settings = getSharedPreferences("PREFS", MODE_PRIVATE);
        boolean currentDay = settings.getBoolean(todayString, false);
        if (!currentDay) {
            perDayCoin = 2000;
            getUpdateCoins(prefManager.getUserToken(),perDayCoin);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(todayString, true);
            editor.apply();
        }
    }

    private void getUpdateCoins(String token, int coin) {
        paramObject = new JsonObject();

        try {
            paramObject.addProperty("Coin", coin);
        } catch (Exception e) {
            e.printStackTrace();
        }

        LoginAPI.login().getUpdateCoins(token,paramObject).enqueue(new retrofit2.Callback<GetTopUsers>() {
            @Override
            public void onResponse(@NonNull Call<GetTopUsers> call, @NonNull Response<GetTopUsers> response) {
                GetTopUsers getTopUsers = response.body();
                Log.e("this", "onResponse: " + getTopUsers.getSuccess());
                assert getTopUsers != null;
                if (getTopUsers.getSuccess() == true) {
                    prefManager.setPerDayCoin(coin);
                  //kin
                  //  Toast.makeText(MainActivity.this, getTopUsers.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    //kin
//                    Toast.makeText(MainActivity.this, getTopUsers.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GetTopUsers> call, Throwable t) {
                //kin
//                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_profile:
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                break;
            case R.id.iv_settings:
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
                break;
            case R.id.iv_about:
                startActivity(new Intent(MainActivity.this, AboutActivity.class));
                break;
            case R.id.ll_liveAnalysis:
                startActivity(new Intent(MainActivity.this, LiveAnalysisActivity.class));
                break;
            case R.id.ll_freeBetting:
                startActivity(new Intent(MainActivity.this, BettingRewardsActivity.class));
                break;
            case R.id.ll_leaderboard:
                startActivity(new Intent(MainActivity.this, LeaderBoardActivity.class));
                break;
                case R.id.ll_enternews:
                    Intent intent = new Intent(getApplicationContext(), RSSFeedActivity.class);
                    intent.putExtra("rssLink", rssLinks.get(0));
                    startActivity(intent);
                    break;
            case R.id.ll_enterInvitation:
                try {
                    String shareText = getResources().getString(R.string.share_text);
                    Intent sendIntent = new Intent();
                    sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shareText + "\nhttps://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                    sendIntent.setType("text/plain");
                    Intent chooseIntent = Intent.createChooser(sendIntent, "Share this via");
                    startActivity(chooseIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
/*

                InstallReferrerClient mReferrerClient;

                mReferrerClient = InstallReferrerClient.newBuilder(this).build();

                mReferrerClient.startConnection(new InstallReferrerStateListener() {
                    @Override
                    public void onInstallReferrerSetupFinished(int responseCode) {
                        switch (responseCode) {
                            case InstallReferrerClient.InstallReferrerResponse.OK:
                                // Connection established
                                try {
                                    ReferrerDetails response =
                                            mReferrerClient.getInstallReferrer();
                                    if (!response.getInstallReferrer().contains("utm_source"))
//                                        edtPRferelCode.setText("" + response.getInstallReferrer());
                                        Log.e("ReferrerCode", "onInstallReferrerSetupFinished: "+ response.getInstallReferrer() );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                mReferrerClient.endConnection();
                                break;
                            case
                                    InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                                // API not available on the current Play Store app
                                break;
                            case
                                    InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                                // Connection could not be established
                                break;
                        }
                    }

                    @Override
                    public void onInstallReferrerServiceDisconnected() {
                        // Try to restart the connection on the next request to
                        // Google Play by calling the startConnection() method.
                    }
                });
*/
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(prefManager.getUserName().equals("")){
            super.onBackPressed();
        }else{
            finishAffinity();
        }
    }


}