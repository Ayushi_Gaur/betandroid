package com.live.productactivation.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.live.productactivation.R;
import com.live.productactivation.activity.BettingDetailsActivity;
import com.live.productactivation.model.MatchListDetail;

import java.util.List;

public class BettingRewardsAdapter extends RecyclerView.Adapter<BettingRewardsAdapter.ViewHolder> {

    private Context context;
    private List<MatchListDetail> liveAnalysisModelList;

    public BettingRewardsAdapter(Context context, List<MatchListDetail> liveAnalysisModelList) {
        this.context = context;
        this.liveAnalysisModelList = liveAnalysisModelList;
    }

    @NonNull
    @Override
    public BettingRewardsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_live_analysis, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BettingRewardsAdapter.ViewHolder holder, int position) {
        holder.tv_game.setText(liveAnalysisModelList.get(position).getGameName());
        holder.tv_duration.setText(String.format("%.2f",Float.parseFloat(liveAnalysisModelList.get(position).getDuration()) / 60));
        holder.tv_spectators.setText(liveAnalysisModelList.get(position).getSpectators());
        holder.ll_gameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, BettingDetailsActivity.class);
                intent.putExtra("game", String.valueOf(liveAnalysisModelList.get(position).getGameName()));
                intent.putExtra("duration", String.valueOf(liveAnalysisModelList.get(position).getDuration()));
                intent.putExtra("spectators", String.valueOf(liveAnalysisModelList.get(position).getSpectators()));
                intent.putExtra("matchid", String.valueOf(liveAnalysisModelList.get(position).getMatchID()));
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return liveAnalysisModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_game, tv_duration, tv_spectators;
        private LinearLayout ll_gameLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_game = itemView.findViewById(R.id.tv_game);
            tv_duration = itemView.findViewById(R.id.tv_duration);
            tv_spectators = itemView.findViewById(R.id.tv_spectators);
            ll_gameLayout = itemView.findViewById(R.id.ll_gameLayout);
        }
    }


}
