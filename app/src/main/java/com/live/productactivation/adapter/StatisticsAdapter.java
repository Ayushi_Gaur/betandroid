package com.live.productactivation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.live.productactivation.R;
import com.live.productactivation.activity.LiveAnalysisDetailsActivity;
import com.live.productactivation.model.Table1;

import java.util.List;

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {
    private Context context;
    private List<Table1> table1List;

    public StatisticsAdapter(Context context, List<Table1> table1List) {
        this.context = context;
        this.table1List = table1List;
    }

    @NonNull
    @Override
    public StatisticsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_statistics, parent, false);
        return new StatisticsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StatisticsAdapter.ViewHolder holder, int position) {
        if(context instanceof LiveAnalysisDetailsActivity){
            for (int i = 0; i < ((LiveAnalysisDetailsActivity)context).heroIDAndNameList.size(); i++) {
                if (((LiveAnalysisDetailsActivity)context).heroIDAndNameList.get(i).getId().equals(table1List.get(position).getHero())) {
                    Glide.with(context).load(((LiveAnalysisDetailsActivity)context).heroIDAndNameList.get(i).getImage()).placeholder(R.drawable.ic_img1).into(holder.iv_heroImage);
                }
            }
        }

        holder.tv_networth.setText(table1List.get(position).getNetworth());
        holder.tv_kPoint.setText(table1List.get(position).getK());
        holder.tv_dPoint.setText(table1List.get(position).getD());
        holder.tv_aPoint.setText(table1List.get(position).getA());
        holder.tv_lhPoint.setText(table1List.get(position).getLh());
        holder.tv_denies.setText(table1List.get(position).getDenies());
        holder.tv_level.setText(table1List.get(position).getLevel());
        holder.tv_gpm.setText(table1List.get(position).getGpm());
        holder.tv_xpm.setText(table1List.get(position).getXpm());
    }

    @Override
    public int getItemCount() {
        return table1List.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_heroImage;
        private TextView tv_networth, tv_kPoint, tv_dPoint, tv_aPoint, tv_lhPoint, tv_denies, tv_level, tv_gpm, tv_xpm;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            iv_heroImage = itemView.findViewById(R.id.iv_heroImage);
            tv_networth = itemView.findViewById(R.id.tv_networth);
            tv_kPoint = itemView.findViewById(R.id.tv_kPoint);
            tv_dPoint = itemView.findViewById(R.id.tv_dPoint);
            tv_aPoint = itemView.findViewById(R.id.tv_aPoint);
            tv_lhPoint = itemView.findViewById(R.id.tv_lhPoint);
            tv_denies = itemView.findViewById(R.id.tv_denies);
            tv_level = itemView.findViewById(R.id.tv_level);
            tv_gpm = itemView.findViewById(R.id.tv_gpm);
            tv_xpm = itemView.findViewById(R.id.tv_xpm);
        }
    }
}
