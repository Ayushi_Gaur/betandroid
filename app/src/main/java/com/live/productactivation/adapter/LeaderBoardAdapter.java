package com.live.productactivation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.live.productactivation.R;
import com.live.productactivation.model.Users;

import java.util.List;

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.ViewHolder> {

    private Context context;
    private List<Users> leaderboardModelList;

    public LeaderBoardAdapter(Context context, List<Users> leaderboardModelList) {
        this.context = context;
        this.leaderboardModelList = leaderboardModelList;
    }

    @NonNull
    @Override
    public LeaderBoardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_leaderboard, parent, false);
        return new LeaderBoardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LeaderBoardAdapter.ViewHolder holder, int position) {
        holder.tv_number.setText(String.valueOf(position+1));
        if(position==0){
            holder.iv_star.setVisibility(View.VISIBLE);
        }
        holder.tv_username.setText(String.valueOf(leaderboardModelList.get(position).getUserId()));
       holder.tv_coins.setText(String.valueOf(leaderboardModelList.get(position).getCoin()));
    }

    @Override
    public int getItemCount() {
        return leaderboardModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

       private TextView tv_number, tv_username, tv_coins;
       private ImageView iv_star;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_number = itemView.findViewById(R.id.tv_number);
            tv_username = itemView.findViewById(R.id.tv_username);
            tv_coins = itemView.findViewById(R.id.tv_coins1);
            iv_star = itemView.findViewById(R.id.iv_star);

        }
    }
}
