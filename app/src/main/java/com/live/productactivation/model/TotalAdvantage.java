package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TotalAdvantage {

    @SerializedName("Draft Advantage Radiant")
    @Expose
    private String draftAdvantageRadiant;

    @SerializedName("Draft Advantage Dire")
    @Expose
    private String draftAdvantageDire;

    @SerializedName("Live Advantage Radiant")
    @Expose
    private String liveAdvantageRadiant;

    @SerializedName("Live Advantage Dire")
    @Expose
    private String liveAdvantageDire;

    public String getDraftAdvantageRadiant() {
        return draftAdvantageRadiant;
    }

    public void setDraftAdvantageRadiant(String draftAdvantageRadiant) {
        this.draftAdvantageRadiant = draftAdvantageRadiant;
    }

    public String getDraftAdvantageDire() {
        return draftAdvantageDire;
    }

    public void setDraftAdvantageDire(String draftAdvantageDire) {
        this.draftAdvantageDire = draftAdvantageDire;
    }

    public String getLiveAdvantageRadiant() {
        return liveAdvantageRadiant;
    }

    public void setLiveAdvantageRadiant(String liveAdvantageRadiant) {
        this.liveAdvantageRadiant = liveAdvantageRadiant;
    }

    public String getLiveAdvantageDire() {
        return liveAdvantageDire;
    }

    public void setLiveAdvantageDire(String liveAdvantageDire) {
        this.liveAdvantageDire = liveAdvantageDire;
    }
}
