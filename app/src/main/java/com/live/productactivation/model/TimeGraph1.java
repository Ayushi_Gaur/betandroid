package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TimeGraph1 {

@SerializedName("Win Chance")
@Expose
private String winChance;

public String getWinChance() {
return winChance;
}

public void setWinChance(String winChance) {
this.winChance = winChance;
}

}