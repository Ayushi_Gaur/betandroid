package com.live.productactivation.model;

public class QuestionsResult {
    private String matchID;
    private String oddtime;
    private String queNumber;
    private String resultNumber;
    private String resultName;
    private String betCoins;
    private String betResult;

    public QuestionsResult(String matchID, String oddtime, String queNumber, String resultNumber, String resultName, String betCoins, String betResult) {
        this.matchID = matchID;
        this.oddtime = oddtime;
        this.queNumber = queNumber;
        this.resultNumber = resultNumber;
        this.resultName = resultName;
        this.betCoins = betCoins;
    }

    public QuestionsResult() {

    }

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(String matchID) {
        this.matchID = matchID;
    }

    public String getOddtime() {
        return oddtime;
    }

    public void setOddtime(String oddtime) {
        this.oddtime = oddtime;
    }

    public String getQueNumber() {
        return queNumber;
    }

    public void setQueNumber(String queNumber) {
        this.queNumber = queNumber;
    }

    public String getResultNumber() {
        return resultNumber;
    }

    public void setResultNumber(String resultNumber) {
        this.resultNumber = resultNumber;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public String getBetCoins() {
        return betCoins;
    }

    public void setBetCoins(String betCoins) {
        this.betCoins = betCoins;
    }

    public String getBetResult() {
        return betResult;
    }

    public void setBetResult(String betResult) {
        this.betResult = betResult;
    }
}
