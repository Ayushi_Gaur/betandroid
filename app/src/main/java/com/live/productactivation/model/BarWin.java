package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BarWin {

@SerializedName("Rwinchance")
@Expose
private String rwinchance;
@SerializedName("Dwinchance")
@Expose
private String dwinchance;

public String getRwinchance() {
return rwinchance;
}

public void setRwinchance(String rwinchance) {
this.rwinchance = rwinchance;
}

public String getDwinchance() {
return dwinchance;
}

public void setDwinchance(String dwinchance) {
this.dwinchance = dwinchance;
}

}