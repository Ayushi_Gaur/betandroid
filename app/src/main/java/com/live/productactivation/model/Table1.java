package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Table1 {

    @SerializedName("hero")
    @Expose
    private String hero;
    @SerializedName("networth")
    @Expose
    private String networth;
    @SerializedName("k")
    @Expose
    private String k;
    @SerializedName("d")
    @Expose
    private String d;
    @SerializedName("a")
    @Expose
    private String a;
    @SerializedName("lh")
    @Expose
    private String lh;
    @SerializedName("denies")
    @Expose
    private String denies;
    @SerializedName("level")
    @Expose
    private String level;
    @SerializedName("gpm")
    @Expose
    private String gpm;
    @SerializedName("xpm")
    @Expose
    private String xpm;


    public String getHero() {
        return hero;
    }

    public void setHero(String hero) {
        this.hero = hero;
    }

    public String getNetworth() {
        return networth;
    }

    public void setNetworth(String networth) {
        this.networth = networth;
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getLh() {
        return lh;
    }

    public void setLh(String lh) {
        this.lh = lh;
    }

    public String getDenies() {
        return denies;
    }

    public void setDenies(String denies) {
        this.denies = denies;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getGpm() {
        return gpm;
    }

    public void setGpm(String gpm) {
        this.gpm = gpm;
    }

    public String getXpm() {
        return xpm;
    }

    public void setXpm(String xpm) {
        this.xpm = xpm;
    }
}