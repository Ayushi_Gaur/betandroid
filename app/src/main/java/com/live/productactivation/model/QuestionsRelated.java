package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class QuestionsRelated {

@SerializedName("Oddtime")
@Expose
private String oddtime;
@SerializedName("Rnetworthodds")
@Expose
private String rnetworthodds;
@SerializedName("Dnetworthodds")
@Expose
private String dnetworthodds;
@SerializedName("Rkodds")
@Expose
private String rkodds;
@SerializedName("Dkodds")
@Expose
private String dkodds;
@SerializedName("Hero1Netodds")
@Expose
private String hero1Netodds;
@SerializedName("Hero2Netodds")
@Expose
private String hero2Netodds;
@SerializedName("Hero3Netodds")
@Expose
private String hero3Netodds;
@SerializedName("Hero4Netodds")
@Expose
private String hero4Netodds;
@SerializedName("Hero5Netodds")
@Expose
private String hero5Netodds;
@SerializedName("Hero6Netodds")
@Expose
private String hero6Netodds;
@SerializedName("Hero7Netodds")
@Expose
private String hero7Netodds;
@SerializedName("Hero8Netodds")
@Expose
private String hero8Netodds;
@SerializedName("Hero9Netodds")
@Expose
private String hero9Netodds;
@SerializedName("Hero10Netodds")
@Expose
private String hero10Netodds;
@SerializedName("Hero1Killodds")
@Expose
private String hero1Killodds;
@SerializedName("Hero2Killodds")
@Expose
private String hero2Killodds;
@SerializedName("Hero3Killodds")
@Expose
private String hero3Killodds;
@SerializedName("Hero4Killodds")
@Expose
private String hero4Killodds;
@SerializedName("Hero5Killodds")
@Expose
private String hero5Killodds;
@SerializedName("Hero6Killodds")
@Expose
private String hero6Killodds;
@SerializedName("Hero7Killodds")
@Expose
private String hero7Killodds;
@SerializedName("Hero8Killodds")
@Expose
private String hero8Killodds;
@SerializedName("Hero9Killodds")
@Expose
private String hero9Killodds;
@SerializedName("Hero10Killodds")
@Expose
private String hero10Killodds;
@SerializedName("TeamNet10")
@Expose
private String teamNet10;
@SerializedName("TeamNet20")
@Expose
private String teamNet20;
@SerializedName("TeamNet30")
@Expose
private String teamNet30;
@SerializedName("TeamNet40")
@Expose
private String teamNet40;
@SerializedName("TeamNet50")
@Expose
private String teamNet50;
@SerializedName("TeamNet60")
@Expose
private String teamNet60;
@SerializedName("TeamNet70")
@Expose
private String teamNet70;
@SerializedName("TeamNet80")
@Expose
private String teamNet80;
@SerializedName("TeamNet90")
@Expose
private String teamNet90;
@SerializedName("TeamNet100")
@Expose
private String teamNet100;
@SerializedName("TeamK10")
@Expose
private String teamK10;
@SerializedName("TeamK20")
@Expose
private String teamK20;
@SerializedName("TeamK30")
@Expose
private String teamK30;
@SerializedName("TeamK40")
@Expose
private String teamK40;
@SerializedName("TeamK50")
@Expose
private String teamK50;
@SerializedName("TeamK60")
@Expose
private String teamK60;
@SerializedName("TeamK70")
@Expose
private String teamK70;
@SerializedName("TeamK80")
@Expose
private String teamK80;
@SerializedName("TeamK90")
@Expose
private String teamK90;
@SerializedName("TeamK100")
@Expose
private String teamK100;
@SerializedName("HeroNet10")
@Expose
private String heroNet10;
@SerializedName("HeroNet20")
@Expose
private String heroNet20;
@SerializedName("HeroNet30")
@Expose
private String heroNet30;
@SerializedName("HeroNet40")
@Expose
private String heroNet40;
@SerializedName("HeroNet50")
@Expose
private String heroNet50;
@SerializedName("HeroNet60")
@Expose
private String heroNet60;
@SerializedName("HeroNet70")
@Expose
private String heroNet70;
@SerializedName("HeroNet80")
@Expose
private String heroNet80;
@SerializedName("HeroNet90")
@Expose
private String heroNet90;
@SerializedName("HeroNet100")
@Expose
private String heroNet100;
@SerializedName("HeroKill10")
@Expose
private String heroKill10;
@SerializedName("HeroKill20")
@Expose
private String heroKill20;
@SerializedName("HeroKill30")
@Expose
private String heroKill30;
@SerializedName("HeroKill40")
@Expose
private String heroKill40;
@SerializedName("HeroKill50")
@Expose
private String heroKill50;
@SerializedName("HeroKill60")
@Expose
private String heroKill60;
@SerializedName("HeroKill70")
@Expose
private String heroKill70;
@SerializedName("HeroKill80")
@Expose
private String heroKill80;
@SerializedName("HeroKill90")
@Expose
private String heroKill90;
@SerializedName("HeroKill100")
@Expose
private String heroKill100;

public String getOddtime() {
return oddtime;
}

public void setOddtime(String oddtime) {
this.oddtime = oddtime;
}

public String getRnetworthodds() {
return rnetworthodds;
}

public void setRnetworthodds(String rnetworthodds) {
this.rnetworthodds = rnetworthodds;
}

public String getDnetworthodds() {
return dnetworthodds;
}

public void setDnetworthodds(String dnetworthodds) {
this.dnetworthodds = dnetworthodds;
}

public String getRkodds() {
return rkodds;
}

public void setRkodds(String rkodds) {
this.rkodds = rkodds;
}

public String getDkodds() {
return dkodds;
}

public void setDkodds(String dkodds) {
this.dkodds = dkodds;
}

public String getHero1Netodds() {
return hero1Netodds;
}

public void setHero1Netodds(String hero1Netodds) {
this.hero1Netodds = hero1Netodds;
}

public String getHero2Netodds() {
return hero2Netodds;
}

public void setHero2Netodds(String hero2Netodds) {
this.hero2Netodds = hero2Netodds;
}

public String getHero3Netodds() {
return hero3Netodds;
}

public void setHero3Netodds(String hero3Netodds) {
this.hero3Netodds = hero3Netodds;
}

public String getHero4Netodds() {
return hero4Netodds;
}

public void setHero4Netodds(String hero4Netodds) {
this.hero4Netodds = hero4Netodds;
}

public String getHero5Netodds() {
return hero5Netodds;
}

public void setHero5Netodds(String hero5Netodds) {
this.hero5Netodds = hero5Netodds;
}

public String getHero6Netodds() {
return hero6Netodds;
}

public void setHero6Netodds(String hero6Netodds) {
this.hero6Netodds = hero6Netodds;
}

public String getHero7Netodds() {
return hero7Netodds;
}

public void setHero7Netodds(String hero7Netodds) {
this.hero7Netodds = hero7Netodds;
}

public String getHero8Netodds() {
return hero8Netodds;
}

public void setHero8Netodds(String hero8Netodds) {
this.hero8Netodds = hero8Netodds;
}

public String getHero9Netodds() {
return hero9Netodds;
}

public void setHero9Netodds(String hero9Netodds) {
this.hero9Netodds = hero9Netodds;
}

public String getHero10Netodds() {
return hero10Netodds;
}

public void setHero10Netodds(String hero10Netodds) {
this.hero10Netodds = hero10Netodds;
}

public String getHero1Killodds() {
return hero1Killodds;
}

public void setHero1Killodds(String hero1Killodds) {
this.hero1Killodds = hero1Killodds;
}

public String getHero2Killodds() {
return hero2Killodds;
}

public void setHero2Killodds(String hero2Killodds) {
this.hero2Killodds = hero2Killodds;
}

public String getHero3Killodds() {
return hero3Killodds;
}

public void setHero3Killodds(String hero3Killodds) {
this.hero3Killodds = hero3Killodds;
}

public String getHero4Killodds() {
return hero4Killodds;
}

public void setHero4Killodds(String hero4Killodds) {
this.hero4Killodds = hero4Killodds;
}

public String getHero5Killodds() {
return hero5Killodds;
}

public void setHero5Killodds(String hero5Killodds) {
this.hero5Killodds = hero5Killodds;
}

public String getHero6Killodds() {
return hero6Killodds;
}

public void setHero6Killodds(String hero6Killodds) {
this.hero6Killodds = hero6Killodds;
}

public String getHero7Killodds() {
return hero7Killodds;
}

public void setHero7Killodds(String hero7Killodds) {
this.hero7Killodds = hero7Killodds;
}

public String getHero8Killodds() {
return hero8Killodds;
}

public void setHero8Killodds(String hero8Killodds) {
this.hero8Killodds = hero8Killodds;
}

public String getHero9Killodds() {
return hero9Killodds;
}

public void setHero9Killodds(String hero9Killodds) {
this.hero9Killodds = hero9Killodds;
}

public String getHero10Killodds() {
return hero10Killodds;
}

public void setHero10Killodds(String hero10Killodds) {
this.hero10Killodds = hero10Killodds;
}

public String getTeamNet10() {
return teamNet10;
}

public void setTeamNet10(String teamNet10) {
this.teamNet10 = teamNet10;
}

public String getTeamNet20() {
return teamNet20;
}

public void setTeamNet20(String teamNet20) {
this.teamNet20 = teamNet20;
}

public String getTeamNet30() {
return teamNet30;
}

public void setTeamNet30(String teamNet30) {
this.teamNet30 = teamNet30;
}

public String getTeamNet40() {
return teamNet40;
}

public void setTeamNet40(String teamNet40) {
this.teamNet40 = teamNet40;
}

public String getTeamNet50() {
return teamNet50;
}

public void setTeamNet50(String teamNet50) {
this.teamNet50 = teamNet50;
}

public String getTeamNet60() {
return teamNet60;
}

public void setTeamNet60(String teamNet60) {
this.teamNet60 = teamNet60;
}

public String getTeamNet70() {
return teamNet70;
}

public void setTeamNet70(String teamNet70) {
this.teamNet70 = teamNet70;
}

public String getTeamNet80() {
return teamNet80;
}

public void setTeamNet80(String teamNet80) {
this.teamNet80 = teamNet80;
}

public String getTeamNet90() {
return teamNet90;
}

public void setTeamNet90(String teamNet90) {
this.teamNet90 = teamNet90;
}

public String getTeamNet100() {
return teamNet100;
}

public void setTeamNet100(String teamNet100) {
this.teamNet100 = teamNet100;
}

public String getTeamK10() {
return teamK10;
}

public void setTeamK10(String teamK10) {
this.teamK10 = teamK10;
}

public String getTeamK20() {
return teamK20;
}

public void setTeamK20(String teamK20) {
this.teamK20 = teamK20;
}

public String getTeamK30() {
return teamK30;
}

public void setTeamK30(String teamK30) {
this.teamK30 = teamK30;
}

public String getTeamK40() {
return teamK40;
}

public void setTeamK40(String teamK40) {
this.teamK40 = teamK40;
}

public String getTeamK50() {
return teamK50;
}

public void setTeamK50(String teamK50) {
this.teamK50 = teamK50;
}

public String getTeamK60() {
return teamK60;
}

public void setTeamK60(String teamK60) {
this.teamK60 = teamK60;
}

public String getTeamK70() {
return teamK70;
}

public void setTeamK70(String teamK70) {
this.teamK70 = teamK70;
}

public String getTeamK80() {
return teamK80;
}

public void setTeamK80(String teamK80) {
this.teamK80 = teamK80;
}

public String getTeamK90() {
return teamK90;
}

public void setTeamK90(String teamK90) {
this.teamK90 = teamK90;
}

public String getTeamK100() {
return teamK100;
}

public void setTeamK100(String teamK100) {
this.teamK100 = teamK100;
}

public String getHeroNet10() {
return heroNet10;
}

public void setHeroNet10(String heroNet10) {
this.heroNet10 = heroNet10;
}

public String getHeroNet20() {
return heroNet20;
}

public void setHeroNet20(String heroNet20) {
this.heroNet20 = heroNet20;
}

public String getHeroNet30() {
return heroNet30;
}

public void setHeroNet30(String heroNet30) {
this.heroNet30 = heroNet30;
}

public String getHeroNet40() {
return heroNet40;
}

public void setHeroNet40(String heroNet40) {
this.heroNet40 = heroNet40;
}

public String getHeroNet50() {
return heroNet50;
}

public void setHeroNet50(String heroNet50) {
this.heroNet50 = heroNet50;
}

public String getHeroNet60() {
return heroNet60;
}

public void setHeroNet60(String heroNet60) {
this.heroNet60 = heroNet60;
}

public String getHeroNet70() {
return heroNet70;
}

public void setHeroNet70(String heroNet70) {
this.heroNet70 = heroNet70;
}

public String getHeroNet80() {
return heroNet80;
}

public void setHeroNet80(String heroNet80) {
this.heroNet80 = heroNet80;
}

public String getHeroNet90() {
return heroNet90;
}

public void setHeroNet90(String heroNet90) {
this.heroNet90 = heroNet90;
}

public String getHeroNet100() {
return heroNet100;
}

public void setHeroNet100(String heroNet100) {
this.heroNet100 = heroNet100;
}

public String getHeroKill10() {
return heroKill10;
}

public void setHeroKill10(String heroKill10) {
this.heroKill10 = heroKill10;
}

public String getHeroKill20() {
return heroKill20;
}

public void setHeroKill20(String heroKill20) {
this.heroKill20 = heroKill20;
}

public String getHeroKill30() {
return heroKill30;
}

public void setHeroKill30(String heroKill30) {
this.heroKill30 = heroKill30;
}

public String getHeroKill40() {
return heroKill40;
}

public void setHeroKill40(String heroKill40) {
this.heroKill40 = heroKill40;
}

public String getHeroKill50() {
return heroKill50;
}

public void setHeroKill50(String heroKill50) {
this.heroKill50 = heroKill50;
}

public String getHeroKill60() {
return heroKill60;
}

public void setHeroKill60(String heroKill60) {
this.heroKill60 = heroKill60;
}

public String getHeroKill70() {
return heroKill70;
}

public void setHeroKill70(String heroKill70) {
this.heroKill70 = heroKill70;
}

public String getHeroKill80() {
return heroKill80;
}

public void setHeroKill80(String heroKill80) {
this.heroKill80 = heroKill80;
}

public String getHeroKill90() {
return heroKill90;
}

public void setHeroKill90(String heroKill90) {
this.heroKill90 = heroKill90;
}

public String getHeroKill100() {
return heroKill100;
}

public void setHeroKill100(String heroKill100) {
this.heroKill100 = heroKill100;
}

}