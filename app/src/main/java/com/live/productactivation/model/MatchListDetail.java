package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MatchListDetail {

    @SerializedName("Match ID")
    @Expose
    private String matchID;
    @SerializedName("Spectators")
    @Expose
    private String spectators;
    @SerializedName("Game Name")
    @Expose
    private String gameName;
    @SerializedName("Duration")
    @Expose
    private String duration;

    public String getMatchID() {
        return matchID;
    }

    public void setMatchID(String matchID) {
        this.matchID = matchID;
    }

    public String getSpectators() {
        return spectators;
    }

    public void setSpectators(String spectators) {
        this.spectators = spectators;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

}