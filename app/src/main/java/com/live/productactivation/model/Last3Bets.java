package com.live.productactivation.model;

public class Last3Bets {
    private String gameName;
    private String oddtime;
    private String netName;
    private String resultName;
    private String betCoins;

    public Last3Bets(String gameName, String oddtime, String netName, String resultName, String betCoins) {
        this.gameName = gameName;
        this.oddtime = oddtime;
        this.netName = netName;
        this.resultName = resultName;
        this.betCoins = betCoins;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getOddtime() {
        return oddtime;
    }

    public void setOddtime(String oddtime) {
        this.oddtime = oddtime;
    }

    public String getNetName() {
        return netName;
    }

    public void setNetName(String netName) {
        this.netName = netName;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }

    public String getBetCoins() {
        return betCoins;
    }

    public void setBetCoins(String betCoins) {
        this.betCoins = betCoins;
    }
}
