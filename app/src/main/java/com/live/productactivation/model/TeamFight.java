package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamFight {

    @SerializedName("Disables Radiant")
    @Expose
    private String disablesRadiant;

    @SerializedName("Disables Dire")
    @Expose
    private String disablesDire;

    @SerializedName("Nuke Dmg Radiant")
    @Expose
    private String nukeDmgRadiant;

    @SerializedName("Nuke Dmg Dire")
    @Expose
    private String nukeDmgDire;

    @SerializedName("R Dmg Radiant")
    @Expose
    private String rDmgRadiant;

    @SerializedName("R Dmg Dire")
    @Expose
    private String rDmgDire;

    @SerializedName("Durability Radiant")
    @Expose
    private String durabilityRadiant;

    @SerializedName("Durability Dire")
    @Expose
    private String durabilityDire;
    @SerializedName("Initiation Radiant")
    @Expose
    private String initiationRadiant;

    @SerializedName("Initiation Dire")
    @Expose
    private String initiationDire;
    @SerializedName("Healing Radiant")
    @Expose
    private String healingRadiant;

    @SerializedName("Healing Dire")
    @Expose
    private String healingDire;

    @SerializedName("Aura Radiant")
    @Expose
    private String auraRadiant;

    @SerializedName("Aura Dire")
    @Expose
    private String auraDire;

    public String getDisablesRadiant() {
        return disablesRadiant;
    }

    public void setDisablesRadiant(String disablesRadiant) {
        this.disablesRadiant = disablesRadiant;
    }

    public String getDisablesDire() {
        return disablesDire;
    }

    public void setDisablesDire(String disablesDire) {
        this.disablesDire = disablesDire;
    }

    public String getNukeDmgRadiant() {
        return nukeDmgRadiant;
    }

    public void setNukeDmgRadiant(String nukeDmgRadiant) {
        this.nukeDmgRadiant = nukeDmgRadiant;
    }

    public String getNukeDmgDire() {
        return nukeDmgDire;
    }

    public void setNukeDmgDire(String nukeDmgDire) {
        this.nukeDmgDire = nukeDmgDire;
    }

    public String getrDmgRadiant() {
        return rDmgRadiant;
    }

    public void setrDmgRadiant(String rDmgRadiant) {
        this.rDmgRadiant = rDmgRadiant;
    }

    public String getrDmgDire() {
        return rDmgDire;
    }

    public void setrDmgDire(String rDmgDire) {
        this.rDmgDire = rDmgDire;
    }

    public String getDurabilityRadiant() {
        return durabilityRadiant;
    }

    public void setDurabilityRadiant(String durabilityRadiant) {
        this.durabilityRadiant = durabilityRadiant;
    }

    public String getDurabilityDire() {
        return durabilityDire;
    }

    public void setDurabilityDire(String durabilityDire) {
        this.durabilityDire = durabilityDire;
    }

    public String getInitiationRadiant() {
        return initiationRadiant;
    }

    public void setInitiationRadiant(String initiationRadiant) {
        this.initiationRadiant = initiationRadiant;
    }

    public String getInitiationDire() {
        return initiationDire;
    }

    public void setInitiationDire(String initiationDire) {
        this.initiationDire = initiationDire;
    }

    public String getHealingRadiant() {
        return healingRadiant;
    }

    public void setHealingRadiant(String healingRadiant) {
        this.healingRadiant = healingRadiant;
    }

    public String getHealingDire() {
        return healingDire;
    }

    public void setHealingDire(String healingDire) {
        this.healingDire = healingDire;
    }

    public String getAuraRadiant() {
        return auraRadiant;
    }

    public void setAuraRadiant(String auraRadiant) {
        this.auraRadiant = auraRadiant;
    }

    public String getAuraDire() {
        return auraDire;
    }

    public void setAuraDire(String auraDire) {
        this.auraDire = auraDire;
    }
}
