package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DraftAdvantage {

    @SerializedName("Disables Radiant")
    @Expose
    private String disablesRadiant;
    @SerializedName("Disables Dire")
    @Expose
    private String disablesDire;
    @SerializedName("Nukes Radiant")
    @Expose
    private String nukesRadiant;
    @SerializedName("Nukes Dire")
    @Expose
    private String nukesDire;
    @SerializedName("Pushing Radiant")
    @Expose
    private String pushingRadiant;
    @SerializedName("Pushing Dire")
    @Expose
    private String pushingDire;
    @SerializedName("Defending Radiant")
    @Expose
    private String defendingRadiant;
    @SerializedName("Defending Dire")
    @Expose
    private String defendingDire;
    @SerializedName("Split Pushing Radiant")
    @Expose
    private String splitPushingRadiant;
    @SerializedName("Split Pushing Dire")
    @Expose
    private String splitPushingDire;
    @SerializedName("R Dmg Radiant")
    @Expose
    private String rDmgRadiant;
    @SerializedName("R Dmg Dire")
    @Expose
    private String rDmgDire;
    @SerializedName("Durability Radiant")
    @Expose
    private String durabilityRadiant;
    @SerializedName("Durability Dire")
    @Expose
    private String durabilityDire;
    @SerializedName("Mobility Radiant")
    @Expose
    private String mobilityRadiant;
    @SerializedName("Mobility Dire")
    @Expose
    private String mobilityDire;
    @SerializedName("Healing Radiant")
    @Expose
    private String healingRadiant;
    @SerializedName("Healing Dire")
    @Expose
    private String healingDire;
    @SerializedName("Initiation Radiant")
    @Expose
    private String initiationRadiant;
    @SerializedName("Initiation Dire")
    @Expose
    private String initiationDire;
    @SerializedName("Counters Radiant")
    @Expose
    private String countersRadiant;
    @SerializedName("Counters Dire")
    @Expose
    private String countersDire;

    public String getDisablesRadiant() {
        return disablesRadiant;
    }

    public void setDisablesRadiant(String disablesRadiant) {
        this.disablesRadiant = disablesRadiant;
    }

    public String getDisablesDire() {
        return disablesDire;
    }

    public void setDisablesDire(String disablesDire) {
        this.disablesDire = disablesDire;
    }

    public String getNukesRadiant() {
        return nukesRadiant;
    }

    public void setNukesRadiant(String nukesRadiant) {
        this.nukesRadiant = nukesRadiant;
    }

    public String getNukesDire() {
        return nukesDire;
    }

    public void setNukesDire(String nukesDire) {
        this.nukesDire = nukesDire;
    }

    public String getPushingRadiant() {
        return pushingRadiant;
    }

    public void setPushingRadiant(String pushingRadiant) {
        this.pushingRadiant = pushingRadiant;
    }

    public String getPushingDire() {
        return pushingDire;
    }

    public void setPushingDire(String pushingDire) {
        this.pushingDire = pushingDire;
    }

    public String getDefendingRadiant() {
        return defendingRadiant;
    }

    public void setDefendingRadiant(String defendingRadiant) {
        this.defendingRadiant = defendingRadiant;
    }

    public String getDefendingDire() {
        return defendingDire;
    }

    public void setDefendingDire(String defendingDire) {
        this.defendingDire = defendingDire;
    }

    public String getSplitPushingRadiant() {
        return splitPushingRadiant;
    }

    public void setSplitPushingRadiant(String splitPushingRadiant) {
        this.splitPushingRadiant = splitPushingRadiant;
    }

    public String getSplitPushingDire() {
        return splitPushingDire;
    }

    public void setSplitPushingDire(String splitPushingDire) {
        this.splitPushingDire = splitPushingDire;
    }

    public String getrDmgRadiant() {
        return rDmgRadiant;
    }

    public void setrDmgRadiant(String rDmgRadiant) {
        this.rDmgRadiant = rDmgRadiant;
    }

    public String getrDmgDire() {
        return rDmgDire;
    }

    public void setrDmgDire(String rDmgDire) {
        this.rDmgDire = rDmgDire;
    }

    public String getDurabilityRadiant() {
        return durabilityRadiant;
    }

    public void setDurabilityRadiant(String durabilityRadiant) {
        this.durabilityRadiant = durabilityRadiant;
    }

    public String getDurabilityDire() {
        return durabilityDire;
    }

    public void setDurabilityDire(String durabilityDire) {
        this.durabilityDire = durabilityDire;
    }

    public String getMobilityRadiant() {
        return mobilityRadiant;
    }

    public void setMobilityRadiant(String mobilityRadiant) {
        this.mobilityRadiant = mobilityRadiant;
    }

    public String getMobilityDire() {
        return mobilityDire;
    }

    public void setMobilityDire(String mobilityDire) {
        this.mobilityDire = mobilityDire;
    }

    public String getHealingRadiant() {
        return healingRadiant;
    }

    public void setHealingRadiant(String healingRadiant) {
        this.healingRadiant = healingRadiant;
    }

    public String getHealingDire() {
        return healingDire;
    }

    public void setHealingDire(String healingDire) {
        this.healingDire = healingDire;
    }

    public String getInitiationRadiant() {
        return initiationRadiant;
    }

    public void setInitiationRadiant(String initiationRadiant) {
        this.initiationRadiant = initiationRadiant;
    }

    public String getInitiationDire() {
        return initiationDire;
    }

    public void setInitiationDire(String initiationDire) {
        this.initiationDire = initiationDire;
    }

    public String getCountersRadiant() {
        return countersRadiant;
    }

    public void setCountersRadiant(String countersRadiant) {
        this.countersRadiant = countersRadiant;
    }

    public String getCountersDire() {
        return countersDire;
    }

    public void setCountersDire(String countersDire) {
        this.countersDire = countersDire;
    }
}
