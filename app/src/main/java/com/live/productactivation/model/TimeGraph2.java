package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeGraph2 {

@SerializedName("Team Fight")
@Expose
private String teamFight;
@SerializedName("Pushing")
@Expose
private String pushing;
@SerializedName("Defending")
@Expose
private String defending;
@SerializedName("Split Pushing")
@Expose
private String splitPushing;
@SerializedName("Live Advantage")
@Expose
private String liveAdvantage;

public String getTeamFight() {
return teamFight;
}

public void setTeamFight(String teamFight) {
this.teamFight = teamFight;
}

public String getPushing() {
return pushing;
}

public void setPushing(String pushing) {
this.pushing = pushing;
}

public String getDefending() {
return defending;
}

public void setDefending(String defending) {
this.defending = defending;
}

public String getSplitPushing() {
return splitPushing;
}

public void setSplitPushing(String splitPushing) {
this.splitPushing = splitPushing;
}

public String getLiveAdvantage() {
return liveAdvantage;
}

public void setLiveAdvantage(String liveAdvantage) {
this.liveAdvantage = liveAdvantage;
}

}