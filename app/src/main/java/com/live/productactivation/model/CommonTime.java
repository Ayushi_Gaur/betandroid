package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommonTime {
    public class JsonDetails {

        @SerializedName("Table 1")
        @Expose
        private List<Table1> table1 = null;
        @SerializedName("Graph1-Draft Advantage")
        @Expose
        private List<DraftAdvantage> graph1DraftAdvantage = null;
        @SerializedName("Graph2-Live Advantage")
        @Expose
        private List<LiveAdvantage> graph2LiveAdvantage = null;
        @SerializedName("Graph3-Total Advantage")
        @Expose
        private List<TotalAdvantage> graph3TotalAdvantage = null;
        @SerializedName("Graph4-Team Fight")
        @Expose
        private List<TeamFight> graph4TeamFight = null;
        @SerializedName("Right Panel")
        @Expose
        private List<Object> rightPanel = null;
        @SerializedName("Basic Info")
        @Expose
        private BasicInfo basicInfo;
        @SerializedName("Questions Related")
        @Expose
        private List<QuestionsRelated> questionsRelated = null;
        @SerializedName("Bar Win")
        @Expose
        private List<BarWin> barWin = null;
        @SerializedName("Common Time")
        @Expose
        private CommonTime commonTime;
        @SerializedName("Time Graph 1")
        @Expose
        private TimeGraph1 timeGraph1;
        @SerializedName("Time Graph 2")
        @Expose
        private TimeGraph2 timeGraph2;
        @SerializedName("Time Graph 3")
        @Expose
        private TimeGraph3 timeGraph3;
        @SerializedName("Time Graph 4")
        @Expose
        private TimeGraph4 timeGraph4;
        @SerializedName("Images for box")
        @Expose
        private List<ImageModel> imagesForBox = null;
        @SerializedName("Hero ID and Names")
        @Expose
        private List<HeroIDAndName> heroIDAndNames = null;

        public List<Table1> getTable1() {
            return table1;
        }

        public void setTable1(List<Table1> table1) {
            this.table1 = table1;
        }

        public List<DraftAdvantage> getGraph1DraftAdvantage() {
            return graph1DraftAdvantage;
        }

        public void setGraph1DraftAdvantage(List<DraftAdvantage> graph1DraftAdvantage) {
            this.graph1DraftAdvantage = graph1DraftAdvantage;
        }

        public List<LiveAdvantage> getGraph2LiveAdvantage() {
            return graph2LiveAdvantage;
        }

        public void setGraph2LiveAdvantage(List<LiveAdvantage> graph2LiveAdvantage) {
            this.graph2LiveAdvantage = graph2LiveAdvantage;
        }

        public List<TotalAdvantage> getGraph3TotalAdvantage() {
            return graph3TotalAdvantage;
        }

        public void setGraph3TotalAdvantage(List<TotalAdvantage> graph3TotalAdvantage) {
            this.graph3TotalAdvantage = graph3TotalAdvantage;
        }

        public List<TeamFight> getGraph4TeamFight() {
            return graph4TeamFight;
        }

        public void setGraph4TeamFight(List<TeamFight> graph4TeamFight) {
            this.graph4TeamFight = graph4TeamFight;
        }

        public List<Object> getRightPanel() {
            return rightPanel;
        }

        public void setRightPanel(List<Object> rightPanel) {
            this.rightPanel = rightPanel;
        }

        public BasicInfo getBasicInfo() {
            return basicInfo;
        }

        public void setBasicInfo(BasicInfo basicInfo) {
            this.basicInfo = basicInfo;
        }

        public List<QuestionsRelated> getQuestionsRelated() {
            return questionsRelated;
        }

        public void setQuestionsRelated(List<QuestionsRelated> questionsRelated) {
            this.questionsRelated = questionsRelated;
        }

        public List<BarWin> getBarWin() {
            return barWin;
        }

        public void setBarWin(List<BarWin> barWin) {
            this.barWin = barWin;
        }

        public CommonTime getCommonTime() {
            return commonTime;
        }

        public void setCommonTime(CommonTime commonTime) {
            this.commonTime = commonTime;
        }

        public TimeGraph1 getTimeGraph1() {
            return timeGraph1;
        }

        public void setTimeGraph1(TimeGraph1 timeGraph1) {
            this.timeGraph1 = timeGraph1;
        }

        public TimeGraph2 getTimeGraph2() {
            return timeGraph2;
        }

        public void setTimeGraph2(TimeGraph2 timeGraph2) {
            this.timeGraph2 = timeGraph2;
        }

        public TimeGraph3 getTimeGraph3() {
            return timeGraph3;
        }

        public void setTimeGraph3(TimeGraph3 timeGraph3) {
            this.timeGraph3 = timeGraph3;
        }

        public TimeGraph4 getTimeGraph4() {
            return timeGraph4;
        }

        public void setTimeGraph4(TimeGraph4 timeGraph4) {
            this.timeGraph4 = timeGraph4;
        }

        public List<ImageModel> getImagesForBox() {
            return imagesForBox;
        }

        public void setImagesForBox(List<ImageModel> imagesForBox) {
            this.imagesForBox = imagesForBox;
        }

        public List<HeroIDAndName> getHeroIDAndNames() {
            return heroIDAndNames;
        }

        public void setHeroIDAndNames(List<HeroIDAndName> heroIDAndNames) {
            this.heroIDAndNames = heroIDAndNames;
        }

    }

    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}