package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageModel {
    @SerializedName("Img")
    @Expose
    private String Img;

    public String getImg() {
        return Img;
    }

    public void setImg(String id) {
        this.Img = Img;
    }
}
