package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeGraph4 {

@SerializedName("Draft Calc")
@Expose
private String draftCalc;
@SerializedName("Death To Output Calc")
@Expose
private String deathToOutputCalc;
@SerializedName("Networth Calc")
@Expose
private String networthCalc;
@SerializedName("Buildings Available Calc")
@Expose
private String buildingsAvailableCalc;
@SerializedName("Live Calc")
@Expose
private String liveCalc;

public String getDraftCalc() {
return draftCalc;
}

public void setDraftCalc(String draftCalc) {
this.draftCalc = draftCalc;
}

public String getDeathToOutputCalc() {
return deathToOutputCalc;
}

public void setDeathToOutputCalc(String deathToOutputCalc) {
this.deathToOutputCalc = deathToOutputCalc;
}

public String getNetworthCalc() {
return networthCalc;
}

public void setNetworthCalc(String networthCalc) {
this.networthCalc = networthCalc;
}

public String getBuildingsAvailableCalc() {
return buildingsAvailableCalc;
}

public void setBuildingsAvailableCalc(String buildingsAvailableCalc) {
this.buildingsAvailableCalc = buildingsAvailableCalc;
}

public String getLiveCalc() {
return liveCalc;
}

public void setLiveCalc(String liveCalc) {
this.liveCalc = liveCalc;
}

}