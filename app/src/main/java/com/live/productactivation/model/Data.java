package com.live.productactivation.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

@SerializedName("UsersList")
@Expose
private List<Users> usersList = null;

public List<Users> getUsersList() {
return usersList;
}

public void setUsersList(List<Users> usersList) {
this.usersList = usersList;
}

}