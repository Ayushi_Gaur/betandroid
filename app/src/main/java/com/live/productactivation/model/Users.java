package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Users {

@SerializedName("UserId")
@Expose
private String userId;
@SerializedName("Image")
@Expose
private String image;
@SerializedName("Coin")
@Expose
private String coin;
@SerializedName("_id")
@Expose
private Integer id;
@SerializedName("created_at")
@Expose
private String createdAt;
@SerializedName("updated_at")
@Expose
private String updatedAt;
@SerializedName("__v")
@Expose
private Integer v;

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getImage() {
return image;
}

public void setImage(String image) {
this.image = image;
}

public String getCoin() {
return coin;
}

public void setCoin(String coin) {
this.coin = coin;
}

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getCreatedAt() {
return createdAt;
}

public void setCreatedAt(String createdAt) {
this.createdAt = createdAt;
}

public String getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(String updatedAt) {
this.updatedAt = updatedAt;
}

public Integer getV() {
return v;
}

public void setV(Integer v) {
this.v = v;
}

}