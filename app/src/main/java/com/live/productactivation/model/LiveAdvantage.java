package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LiveAdvantage {

    @SerializedName("Team Fight Radiant")
    @Expose
    private String teamFightRadiant;

    @SerializedName("Team Fight Dire")
    @Expose
    private String teamFightDire;

    @SerializedName("Pushing Radiant")
    @Expose
    private String pushingRadiant;

    @SerializedName("Pushing Dire")
    @Expose
    private String pushingDire;

    @SerializedName("Defending Radiant")
    @Expose
    private String defendingRadiant;

    @SerializedName("Defending Dire")
    @Expose
    private String defendingDire;

    @SerializedName("Split Pushing Radiant")
    @Expose
    private String splitPushingRadiant;

    @SerializedName("Split Pushing Dire")
    @Expose
    private String splitPushingDire;

    public String getTeamFightRadiant() {
        return teamFightRadiant;
    }

    public void setTeamFightRadiant(String teamFightRadiant) {
        this.teamFightRadiant = teamFightRadiant;
    }

    public String getTeamFightDire() {
        return teamFightDire;
    }

    public void setTeamFightDire(String teamFightDire) {
        this.teamFightDire = teamFightDire;
    }

    public String getPushingRadiant() {
        return pushingRadiant;
    }

    public void setPushingRadiant(String pushingRadiant) {
        this.pushingRadiant = pushingRadiant;
    }

    public String getPushingDire() {
        return pushingDire;
    }

    public void setPushingDire(String pushingDire) {
        this.pushingDire = pushingDire;
    }

    public String getDefendingRadiant() {
        return defendingRadiant;
    }

    public void setDefendingRadiant(String defendingRadiant) {
        this.defendingRadiant = defendingRadiant;
    }

    public String getDefendingDire() {
        return defendingDire;
    }

    public void setDefendingDire(String defendingDire) {
        this.defendingDire = defendingDire;
    }

    public String getSplitPushingRadiant() {
        return splitPushingRadiant;
    }

    public void setSplitPushingRadiant(String splitPushingRadiant) {
        this.splitPushingRadiant = splitPushingRadiant;
    }

    public String getSplitPushingDire() {
        return splitPushingDire;
    }

    public void setSplitPushingDire(String splitPushingDire) {
        this.splitPushingDire = splitPushingDire;
    }
}
