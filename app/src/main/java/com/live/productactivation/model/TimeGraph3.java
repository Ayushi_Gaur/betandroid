package com.live.productactivation.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TimeGraph3 {

@SerializedName("Disables")
@Expose
private String disables;
@SerializedName("Nuke Dmg")
@Expose
private String nukeDmg;
@SerializedName("R Dmg")
@Expose
private String rDmg;
@SerializedName("Durability")
@Expose
private String durability;
@SerializedName("Initiation")
@Expose
private String initiation;
@SerializedName("Healing")
@Expose
private String healing;
@SerializedName("Aura")
@Expose
private String aura;
@SerializedName("Team Fight total")
@Expose
private String teamFightTotal;
@SerializedName("Mobility")
@Expose
private String mobility;
@SerializedName("Fast Farmer")
@Expose
private String fastFarmer;

public String getDisables() {
return disables;
}

public void setDisables(String disables) {
this.disables = disables;
}

public String getNukeDmg() {
return nukeDmg;
}

public void setNukeDmg(String nukeDmg) {
this.nukeDmg = nukeDmg;
}

public String getRDmg() {
return rDmg;
}

public void setRDmg(String rDmg) {
this.rDmg = rDmg;
}

public String getDurability() {
return durability;
}

public void setDurability(String durability) {
this.durability = durability;
}

public String getInitiation() {
return initiation;
}

public void setInitiation(String initiation) {
this.initiation = initiation;
}

public String getHealing() {
return healing;
}

public void setHealing(String healing) {
this.healing = healing;
}

public String getAura() {
return aura;
}

public void setAura(String aura) {
this.aura = aura;
}

public String getTeamFightTotal() {
return teamFightTotal;
}

public void setTeamFightTotal(String teamFightTotal) {
this.teamFightTotal = teamFightTotal;
}

public String getMobility() {
return mobility;
}

public void setMobility(String mobility) {
this.mobility = mobility;
}

public String getFastFarmer() {
return fastFarmer;
}

public void setFastFarmer(String fastFarmer) {
this.fastFarmer = fastFarmer;
}

}