package com.live.mpchartlib.interfaces.dataprovider;

import com.live.mpchartlib.components.YAxis;
import com.live.mpchartlib.data.LineData;

public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {

    LineData getLineData();

    YAxis getAxis(YAxis.AxisDependency dependency);
}
