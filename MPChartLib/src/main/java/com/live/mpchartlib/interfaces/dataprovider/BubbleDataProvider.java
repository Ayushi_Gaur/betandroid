package com.live.mpchartlib.interfaces.dataprovider;

import com.live.mpchartlib.data.BubbleData;

public interface BubbleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    BubbleData getBubbleData();
}
