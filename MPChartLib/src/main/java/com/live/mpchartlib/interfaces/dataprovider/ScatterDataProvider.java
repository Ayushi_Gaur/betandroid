package com.live.mpchartlib.interfaces.dataprovider;

import com.live.mpchartlib.data.ScatterData;

public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {

    ScatterData getScatterData();
}
