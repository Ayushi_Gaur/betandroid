package com.live.mpchartlib.interfaces.dataprovider;

import com.live.mpchartlib.components.YAxis.AxisDependency;
import com.live.mpchartlib.data.BarLineScatterCandleBubbleData;
import com.live.mpchartlib.utils.Transformer;

public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {

    Transformer getTransformer(AxisDependency axis);
    boolean isInverted(AxisDependency axis);
    
    float getLowestVisibleX();
    float getHighestVisibleX();

    BarLineScatterCandleBubbleData getData();
}
