package com.live.mpchartlib.interfaces.dataprovider;

import com.live.mpchartlib.data.CandleData;

public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider {

    CandleData getCandleData();
}
